SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";
CREATE DATABASE IF NOT EXISTS `unieatdb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `unieatdb`;

DELIMITER $$
DROP FUNCTION IF EXISTS `levenshtein`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `levenshtein` (`s1` TEXT, `s2` TEXT) RETURNS INT(11) BEGIN
    DECLARE s1_len, s2_len, i, j, c, c_temp, cost INT;
    DECLARE s1_char CHAR;
    DECLARE cv0, cv1 text;
    SET s1_len = CHAR_LENGTH(s1), s2_len = CHAR_LENGTH(s2), cv1 = 0x00, j = 1, i = 1, c = 0;
    IF s1 = s2 THEN
      RETURN 0;
    ELSEIF s1_len = 0 THEN
      RETURN s2_len;
    ELSEIF s2_len = 0 THEN
      RETURN s1_len;
    ELSE
      WHILE j <= s2_len DO
        SET cv1 = CONCAT(cv1, UNHEX(HEX(j))), j = j + 1;
      END WHILE;
      WHILE i <= s1_len DO
        SET s1_char = SUBSTRING(s1, i, 1), c = i, cv0 = UNHEX(HEX(i)), j = 1;
        WHILE j <= s2_len DO
          SET c = c + 1;
          IF s1_char = SUBSTRING(s2, j, 1) THEN
            SET cost = 0; ELSE SET cost = 1;
          END IF;
          SET c_temp = CONV(HEX(SUBSTRING(cv1, j, 1)), 16, 10) + cost;
          IF c > c_temp THEN SET c = c_temp; END IF;
            SET c_temp = CONV(HEX(SUBSTRING(cv1, j+1, 1)), 16, 10) + 1;
            IF c > c_temp THEN
              SET c = c_temp;
            END IF;
            SET cv0 = CONCAT(cv0, UNHEX(HEX(c))), j = j + 1;
        END WHILE;
        SET cv1 = cv0, i = i + 1;
      END WHILE;
    END IF;
    RETURN c;
  END$$

DELIMITER ;

DROP TABLE IF EXISTS `administrators`;
CREATE TABLE `administrators` (
  `UserID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP VIEW IF EXISTS `approvedsuppliers`;
CREATE TABLE `approvedsuppliers` (
`UserID` int(11)
,`ShopName` varchar(50)
,`ShortDescription` varchar(100)
,`LongDescription` varchar(800)
,`Address` varchar(60)
,`PhoneNumber` varchar(13)
,`Approved` tinyint(4)
);
DROP VIEW IF EXISTS `availableproducts`;
CREATE TABLE `availableproducts` (
`ProductID` int(11)
,`Name` varchar(20)
,`Description` varchar(500)
,`Price` float
,`Available` tinyint(4)
,`SupplierID` int(11)
,`CategoryID` int(11)
);

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `UserID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Surname` varchar(20) NOT NULL,
  `PhoneNumber` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `NotificationID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `NotificationDateTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `NotificationType` varchar(30) NOT NULL,
  `Viewed` tinyint(1) NOT NULL DEFAULT '0',
  `OrderID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `OrderID` int(11) NOT NULL,
  `RequestDateTime` datetime DEFAULT NULL,
  `DeliveryDateTime` datetime DEFAULT NULL,
  `Location` varchar(30) DEFAULT NULL,
  `State` varchar(30) NOT NULL,
  `SupplierID` int(11) NOT NULL,
  `CustomerID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TRIGGER IF EXISTS `after_orders_update`;
DELIMITER $$
CREATE TRIGGER `after_orders_update` AFTER UPDATE ON `orders` FOR EACH ROW BEGIN

    IF NEW.State = 'Sent'
    THEN
        INSERT INTO notifications
        SET
        NotificationType = 'OrderReceived',
        UserID = NEW.SupplierID,
        OrderID = NEW.OrderID;

    ELSEIF NEW.State = 'Accepted'
    THEN
        INSERT INTO notifications
        SET
        NotificationType = 'OrderAccepted',
        UserID = NEW.CustomerID,
        OrderID = NEW.OrderID;

    ELSEIF NEW.State = 'Delivering'
    THEN
        INSERT INTO notifications
        SET
        NotificationType = 'OrderDelivering',
        UserID = NEW.CustomerID,
        OrderID = NEW.OrderID;

    ELSEIF NEW.State = 'Delivered'
    THEN
        INSERT INTO notifications
        SET
        NotificationType = 'OrderDelivered',
        UserID = NEW.CustomerID,
        OrderID = NEW.OrderID;

    ELSEIF NEW.State = 'Invalid'
    THEN
        INSERT INTO notifications
        SET
        NotificationType = 'OrderInvalidated',
        UserID = NEW.CustomerID,
        OrderID = NEW.OrderID;

    ELSEIF NEW.State = 'Declined'
    THEN
        INSERT INTO notifications
        SET
        NotificationType = 'OrderDeclined',
        UserID = NEW.CustomerID,
        OrderID = NEW.OrderID;
    END IF;

END
$$
DELIMITER ;

DROP TABLE IF EXISTS `productcategories`;
CREATE TABLE `productcategories` (
  `CategoryID` int(11) NOT NULL,
  `CategoryName` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `productrequests`;
CREATE TABLE `productrequests` (
  `ProductRequestID` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL,
  `Notes` varchar(500) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `OrderID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TRIGGER IF EXISTS `after_productrequests_delete`;
DELIMITER $$
CREATE TRIGGER `after_productrequests_delete` AFTER DELETE ON `productrequests` FOR EACH ROW DELETE FROM orders
WHERE OLD.OrderID = OrderID
AND NOT EXISTS (
    SELECT ProductRequestID
    FROM productrequests
    WHERE OLD.OrderID = OrderID)
$$
DELIMITER ;

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `ProductID` int(11) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `Price` float NOT NULL,
  `Available` tinyint(4) NOT NULL DEFAULT '1',
  `SupplierID` int(11) NOT NULL,
  `CategoryID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TRIGGER IF EXISTS `after_products_update`;
DELIMITER $$
CREATE TRIGGER `after_products_update` AFTER UPDATE ON `products` FOR EACH ROW IF NEW.Available <> OLD.Available AND NEW.Available = 0
THEN
	INSERT INTO notifications
    (NotificationType, UserID, OrderID)
    SELECT DISTINCT 'OrderInvalidated', O.CustomerID, O.OrderID
    FROM Orders O, productrequests R
    WHERE O.OrderID = R.OrderID
    AND R.ProductID = NEW.ProductID
    AND O.State = 'Sent';

    UPDATE orders
    SET State = 'Invalid'
    WHERE State = 'Sent'
    AND OrderID IN (
        SELECT OrderID
        FROM productrequests
        WHERE ProductID = NEW.ProductID
    );

    DELETE FROM productrequests
    WHERE ProductID = NEW.ProductID
    AND (SELECT State
         FROM orders
         WHERE orders.OrderID = productrequests.OrderID) = 'Cart';
END IF
$$
DELIMITER ;
DROP VIEW IF EXISTS `rankedproducts`;
CREATE TABLE `rankedproducts` (
`ProductID` int(11)
,`Name` varchar(20)
,`Price` float
,`Description` varchar(500)
,`SupplierID` int(11)
,`CategoryID` int(11)
,`Ranking` bigint(21)
);
DROP VIEW IF EXISTS `rankedsuppliers`;
CREATE TABLE `rankedsuppliers` (
`UserID` int(11)
,`ShopName` varchar(50)
,`ShortDescription` varchar(100)
,`Ranking` bigint(21)
);

DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE `suppliers` (
  `UserID` int(11) NOT NULL,
  `ShopName` varchar(50) NOT NULL,
  `ShortDescription` varchar(100) NOT NULL,
  `LongDescription` varchar(800) NOT NULL,
  `Address` varchar(60) NOT NULL,
  `PhoneNumber` varchar(13) NOT NULL,
  `Approved` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `useraccounts`;
CREATE TABLE `useraccounts` (
  `UserID` int(11) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `PasswordHash` char(128) NOT NULL,
  `PasswordSalt` char(128) NOT NULL,
  `UserType` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `approvedsuppliers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `approvedsuppliers`  AS  select `suppliers`.`UserID` AS `UserID`,`suppliers`.`ShopName` AS `ShopName`,`suppliers`.`ShortDescription` AS `ShortDescription`,`suppliers`.`LongDescription` AS `LongDescription`,`suppliers`.`Address` AS `Address`,`suppliers`.`PhoneNumber` AS `PhoneNumber`,`suppliers`.`Approved` AS `Approved` from `suppliers` where (`suppliers`.`Approved` = 1) ;
DROP TABLE IF EXISTS `availableproducts`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `availableproducts`  AS  select `products`.`ProductID` AS `ProductID`,`products`.`Name` AS `Name`,`products`.`Description` AS `Description`,`products`.`Price` AS `Price`,`products`.`Available` AS `Available`,`products`.`SupplierID` AS `SupplierID`,`products`.`CategoryID` AS `CategoryID` from `products` where (`products`.`Available` = 1) ;
DROP TABLE IF EXISTS `rankedproducts`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `rankedproducts`  AS  select `p`.`ProductID` AS `ProductID`,`p`.`Name` AS `Name`,`p`.`Price` AS `Price`,`p`.`Description` AS `Description`,`p`.`SupplierID` AS `SupplierID`,`p`.`CategoryID` AS `CategoryID`,count(`r`.`ProductRequestID`) AS `Ranking` from (`availableproducts` `p` left join (`productrequests` `r` join `orders` `o` on(((`r`.`OrderID` = `o`.`OrderID`) and (`o`.`State` = 'Delivered')))) on((`p`.`ProductID` = `r`.`ProductID`))) group by `p`.`ProductID`,`p`.`Name`,`p`.`Price`,`p`.`Description`,`p`.`SupplierID`,`p`.`CategoryID` ;
DROP TABLE IF EXISTS `rankedsuppliers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `rankedsuppliers`  AS  select `s`.`UserID` AS `UserID`,`s`.`ShopName` AS `ShopName`,`s`.`ShortDescription` AS `ShortDescription`,count(`o`.`OrderID`) AS `Ranking` from (`approvedsuppliers` `s` left join `orders` `o` on(((`s`.`UserID` = `o`.`SupplierID`) and (`o`.`State` = 'Delivered')))) group by `s`.`UserID`,`s`.`ShopName`,`s`.`ShortDescription` ;


ALTER TABLE `administrators`
  ADD PRIMARY KEY (`UserID`);

ALTER TABLE `customers`
  ADD PRIMARY KEY (`UserID`);

ALTER TABLE `notifications`
  ADD PRIMARY KEY (`NotificationID`),
  ADD KEY `FKProduces` (`OrderID`),
  ADD KEY `FKReceiver` (`UserID`);

ALTER TABLE `orders`
  ADD PRIMARY KEY (`OrderID`),
  ADD KEY `FKFrom` (`SupplierID`),
  ADD KEY `FKRequests` (`CustomerID`);

ALTER TABLE `productcategories`
  ADD PRIMARY KEY (`CategoryID`);

ALTER TABLE `productrequests`
  ADD PRIMARY KEY (`ProductRequestID`),
  ADD KEY `FKFor` (`ProductID`),
  ADD KEY `FKComposition` (`OrderID`);

ALTER TABLE `products`
  ADD PRIMARY KEY (`ProductID`),
  ADD KEY `FKOffers` (`SupplierID`),
  ADD KEY `FKCategory` (`CategoryID`);

ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`UserID`);

ALTER TABLE `useraccounts`
  ADD PRIMARY KEY (`UserID`),
  ADD UNIQUE KEY `Username` (`Username`);


ALTER TABLE `notifications`
  MODIFY `NotificationID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `orders`
  MODIFY `OrderID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `productcategories`
  MODIFY `CategoryID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `productrequests`
  MODIFY `ProductRequestID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `products`
  MODIFY `ProductID` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `useraccounts`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `administrators`
  ADD CONSTRAINT `FKAdministratorUser` FOREIGN KEY (`UserID`) REFERENCES `useraccounts` (`UserID`);

ALTER TABLE `customers`
  ADD CONSTRAINT `FKCustomerUser` FOREIGN KEY (`UserID`) REFERENCES `useraccounts` (`UserID`);

ALTER TABLE `notifications`
  ADD CONSTRAINT `FKProduces` FOREIGN KEY (`OrderID`) REFERENCES `orders` (`OrderID`) ON DELETE CASCADE,
  ADD CONSTRAINT `FKReceiver` FOREIGN KEY (`UserID`) REFERENCES `useraccounts` (`UserID`) ON DELETE CASCADE;

ALTER TABLE `orders`
  ADD CONSTRAINT `FKFrom` FOREIGN KEY (`SupplierID`) REFERENCES `suppliers` (`UserID`),
  ADD CONSTRAINT `FKRequests` FOREIGN KEY (`CustomerID`) REFERENCES `customers` (`UserID`);

ALTER TABLE `productrequests`
  ADD CONSTRAINT `FKComposition` FOREIGN KEY (`OrderID`) REFERENCES `orders` (`OrderID`),
  ADD CONSTRAINT `FKFor` FOREIGN KEY (`ProductID`) REFERENCES `products` (`ProductID`);

ALTER TABLE `products`
  ADD CONSTRAINT `FKCategory` FOREIGN KEY (`CategoryID`) REFERENCES `productcategories` (`CategoryID`),
  ADD CONSTRAINT `FKOffers` FOREIGN KEY (`SupplierID`) REFERENCES `suppliers` (`UserID`);

ALTER TABLE `suppliers`
  ADD CONSTRAINT `FKSupplierUser` FOREIGN KEY (`UserID`) REFERENCES `useraccounts` (`UserID`);
COMMIT;
