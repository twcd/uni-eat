<?php
include "dbUtils.php";
include "secureSession.php";
include "errorChecker.php";
sec_session_start();
login_check($mysqli);

if (userLoggedIn() and isSupplier()) {
    $userID = getUserID();
} else {
    header("Location: home.php");
    exit;
}
$result = getSupplierData($userID, $username, $email, $name, $shortDesc, $longDesc, $address, $phone, $mysqli);

?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <script type="text/javascript" src="scripts/showPath.js"></script>
        <title><?php echo $username ?> - UniEat - Modifica profilo</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <?php if ($errNo == 1) { ?>
                    <div class="alert alert-danger" role="alert">
                        Mancano alcuni parametri.
                    </div>
                <?php } else if ($errNo == 2) { ?>
                    <div class="alert alert-danger" role="alert">
                        Impossibile aggiornare il profilo.
                    </div>
                <?php } else if ($errNo == 3) { ?>
                    <div class="alert alert-danger" role="alert">
                        Devi essere un cliente per usare questa funzionalità.
                    </div>
                <?php } else if ($errNo == 4) { ?>
                    <div class="alert alert-danger" role="alert">
                        Il profilo è stato aggiornato ma non è stato possibile salvare le immagini.
                    </div>
                <?php } ?>
                <form action="processSupplierProfileUpdate.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="supplierID" value="<?php echo $userID ?>" />
                    <div class="form-group">
                        <label for="shopImage">Immagine negozio</label>
                        <div class="custom-file">
                            <input id="imageFile" type="file" class="custom-file-input" name="imageSrc[]" multiple>
                            <label class="custom-file-label" for="imageFile">Scegli un file...</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nameInput">Nome fornitore</label>
                        <input id="nameInput" class="form-control" type="text" name="name" value="<?php echo $name ?>"/>
                    </div>
                    <div class="form-group">
                        <label for="emailInput">Email fornitore</label>
                        <input id="emailInput" class="form-control" type="text" name="email" value="<?php echo $email ?>"/>
                    </div>
                    <div class="form-group">
                        <label for="shortDescInput">Descrizione breve</label>
                        <textarea class="form-control" id="shortDescInput" name="shortDesc" rows="2"><?php echo $shortDesc ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="longDescInput">Descrizione Completa</label>
                        <textarea class="form-control" id="longDescInput" name="longDesc" rows="4"><?php echo $longDesc ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="addressInput">Indirizzo</label>
                        <input id="addressInput" class="form-control" type="text" name="address" value="<?php echo $address ?>"/>
                    </div>
                    <div class="form-group">
                        <label for="phoneInput">Telefono</label>
                        <input id="phoneInput" class="form-control" type="text" name="phone" value="<?php echo $phone ?>"/>
                    </div>
                    <input class="btn btn-primary" type="submit" value="Aggiorna"/>
                    <a class="btn btn-secondary" href="supplier.php">Annulla</a>
                </form>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
