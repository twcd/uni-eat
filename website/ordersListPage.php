<?php
include "dbUtils.php";
include "secureSession.php";
include "misc.php";
sec_session_start();
login_check($mysqli);
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <script type="text/javascript" src="scripts/supplierOrders.js"></script>
        <title>Ordini - UniEat</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <?php if (!userLoggedIn()) { ?>
                    <div class="alert alert-danger" role="alert">
                        Devi esserti autenticato per accedere a questa pagina.
                    </div>
                <?php } else { ?>
                    <section>
                        <h1>I miei ordini</h1>
                        <?php if(isSupplier()) {
                            getSupplierOrders(getUserID(), $orders, $mysqli);
                        }
                        if(isCustomer()){
                            getCustomerOrders(getUserID(), $orders, $mysqli);
                        }
                        if(count($orders) > 0) { ?>
                            <div class="accordion" id="accordionOrders">
                                <?php foreach($orders as $order) { ?>
                                    <div class="orderLine mt-2">
                                        <span hidden class="status"><?php echo $order['State']?></span>
                                        <span hidden class="orderID"><?php echo $order['OrderID']?></span>
                                        <button class="btnOrderStatus btn btn-light btn-lg btn-block" id="<?php echo "orderHeader".$order['OrderID']?>" type="button" data-toggle="collapse" data-target="<?php echo "#collapseOrder".$order['OrderID'] ?>" aria-expanded="false" aria-controls="<?php echo "collapseOrder".$order['OrderID'] ?>">
                                            <div class="d-flex flex-column flex-sm-row justify-content-between">
                                                <div class="orderDate"><?php echo formatDateTime($order['DeliveryDateTime']) ?></div>
                                                <div class="orderState"></div>
                                            </div>
                                        </button>
                                        <div id="<?php echo "collapseOrder".$order['OrderID'] ?>" class="collapse" aria-labelledby="<?php echo "orderHeader".$order['OrderID']?>" data-parent="#accordionOrders">
                                            <div class="p-2">
                                                <div class="text-center">
                                                    <a class="btn btn-link" href="<?php echo "order.php?id=".$order['OrderID'] ?>">Visualizza Ordine</a>
                                                    <div class="row">
                                                        <div class="col-sm text-sm-right">
                                                            <strong>Data consegna</strong>
                                                        </div>
                                                        <div class="col-sm text-sm-left">
                                                            <?php echo formatDateTime($order['DeliveryDateTime']) ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm text-sm-right">
                                                            <strong>Data richiesta</strong>
                                                        </div>
                                                        <div class="col-sm text-sm-left">
                                                            <?php echo formatDateTime($order['RequestDateTime']) ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm text-sm-right">
                                                            <strong>Consegna presso</strong>
                                                        </div>
                                                        <div class="col-sm text-sm-left">
                                                            <?php echo $order['Location'] ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php if(isSupplier()) { ?>
                                                    <div class="row py-1">
                                                        <div class="col-md my-1">
                                                            <button type="button" class="acceptOrder btn-primary btn btn-sm btn-block">Accetta</button>
                                                        </div>
                                                        <div class="col-md my-1">
                                                            <button type="button" class="declineOrder btn-primary btn btn-sm btn-block">Rifiuta</button>
                                                        </div>
                                                        <div class="col-md my-1">
                                                            <button type="button" class="orderSent btn-primary btn btn-sm btn-block">Spedito</button>
                                                        </div>
                                                        <div class="col-md my-1">
                                                            <button type="button" class="orderDelivered btn-primary btn btn-sm btn-block">Consegnato</button>
                                                        </div>
                                                    </div>
                                                <?php }?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } else { ?>
                            <div class="alert alert-info" role="alert">
                                Sembra che nessuno abbia ancora effettuato ordini.
                            </div>
                        <?php } ?>
                    </section>
                <?php } ?>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
