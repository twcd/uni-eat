<?php
include "dbUtils.php";
include "secureSession.php";
include "fileSystemUtils.php";
include "misc.php";
sec_session_start();
login_check($mysqli);
$total = 0;
$orderID = $_GET['id'];
if (!isset($orderID)) {
    $errorMessage = "Nessun ID ordine specificato.";
} else if (!getOrderDetails($orderID, $details, $mysqli) or !getOrderRequests($orderID, $requests, $mysqli)) {
    $errorMessage = "Impossibile recuperare i dati degli ordini.";
} else if ($details['CustomerID'] != getUserID() and $details['SupplierID'] != getUserID()) {
    $errorMessage = "L'ordine specificato appartiene a un altro utente.";
}
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <script type="text/javascript" src="scripts/orders.js"></script>
        <title>Dettagli ordine - UniEat</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <section>
                    <h1 class="text-center">Dettagli ordine</h1>
                    <?php if (isset($errorMessage)) { ?>
                        <div class="alert alert-danger" role="alert">
                            <?php echo $errorMessage ?>
                        </div>
                    <?php } else {
                        if(isSupplier()){ ?>
                            <div class="row py-1">
                                <span hidden class="status"><?php echo $details['State']?></span>
                                <span hidden class="orderID"><?php echo $details['OrderID']?></span>
                                <div class="col-md-3 my-1">
                                    <button type="button" class="acceptOrder btn-primary btn btn-block">Accetta</button>
                                </div>
                                <div class="col-md-3 my-1">
                                    <button type="button" class="declineOrder btn-primary btn btn-block">Rifiuta</button>
                                </div>
                                <div class="col-md-3 my-1">
                                    <button type="button" class="orderSent btn-primary btn btn-block">Spedito</button>
                                </div>
                                <div class="col-md-3 my-1">
                                    <button type="button" class="orderDelivered btn-primary btn btn-block">Consegnato</button>
                                </div>
                            </div>
                        <?php } ?>
                        <section class="text-center">
                            <h2 class="border-bottom">Informazioni generali</h2>
                            <p>
                                <strong>Stato</strong>:
                                <span class="actualState">
                                    <?php echo orderStateString($details['State']) ?>
                                </span>
                                <br/>
                                <strong>Data e ora consegna</strong>: <?php echo formatDateTime($details['DeliveryDateTime']) ?>
                                <br/>
                                <strong>Data e ora richiesta</strong>: <?php echo formatDateTime($details['RequestDateTime']) ?>
                                <?php if (isCustomer()) { ?>
                                    <br/>
                                    <strong>Fornitore</strong>: <a href="<?php echo "supplier.php?id=".$details['SupplierID'] ?>"><?php echo $details['ShopName'] ?></a>
                                <?php } else if (isSupplier()) { ?>
                                    <br/>
                                    <strong>Nome e cognome del cliente</strong>: <?php echo $details['CustomerName']." ".$details['CustomerSurname'] ?>
                                    <br/>
                                    <strong>Numero di telefono</strong>: <?php echo $details['CustomerPhoneNumber'] ?>
                                <?php } ?>
                            </p>
                        </section>
                        <section>
                            <h2 class="border-bottom text-center">Elenco prodotti</h2>
                            <div>
                                <?php foreach($requests as $r) {
                                    $localTotal = $r['Quantity'] * $r['Price'];
                                    $total += $localTotal;
                                    ?>
                                    <div class="lineRow d-flex p-2 p-md-3 flex-row">
                                        <div class="col-2 p-0">
                                            <div class="d-flex flex-column justify-content-center h-100">
                                                <div class="thumbnail">
                                                    <img class="center-cropped rounded-thumbnail" src="<?php echo getProductImagePath($r['ProductID'], "") ?>" alt="Immagine di <?php echo $r['Name']?>"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6 px-2">
                                            <div class="d-flex flex-row align-items-center justify-content-between">
                                                <div class="productName text-truncate">
                                                    <strong><?php echo $r['Name']?></strong>
                                                </div>
                                            </div>
                                            <div class="item-price">
                                                <?php echo money($r['Price']) ?>
                                            </div>
                                            <div class="w-100 text-truncate">
                                                <?php echo strlen($r['Notes']) == 0 ? "-" : $r['Notes'] ?>
                                            </div>
                                        </div>
                                        <div class="col-4 p-0">
                                            <div class="d-flex flex-column align-items-end">
                                                <span><strong>Quantità</strong></span>
                                                <span><?php echo $r['Quantity'] ?></span>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </section>
                        <p class="border-top border-bottom h4 py-3 mt-2 text-center">Totale: <span id="cartTotal" class="total-display"><?php echo money($total) ?></span></p>
                    <?php } ?>
                </section>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
