<?php
include "dbUtils.php";
include "secureSession.php";
include "errorChecker.php";
sec_session_start();
login_check($mysqli);

?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <script type="text/javascript" src="scripts/showPath.js"></script>
        <title></title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <?php if (!userLoggedIn() || !isSupplier()) { ?>
                    <div class="alert alert-danger" role="alert">
                        Devi esserti autenticato come fornitore per accedere a questa pagina.
                    </div>
                <?php } else { ?>
                    <section>
                        <?php if(getSingleProduct($_GET['productID'], $product, $mysqli)) {?>
                            <h1>Modifica prodotto</h1>
                            <?php if ($errNo == 1) { ?>
                                <div class="alert alert-danger" role="alert">
                                    Mancano alcuni parametri.
                                </div>
                            <?php } else if ($errNo == 2) { ?>
                                <div class="alert alert-danger" role="alert">
                                    Impossibile aggiornare il prodotto.
                                </div>
                            <?php } else if ($errNo == 3) { ?>
                                <div class="alert alert-danger" role="alert">
                                    Devi essere un fornitore per usare questa funzionalità.
                                </div>
                            <?php } else if ($errNo == 4) { ?>
                                <div class="alert alert-danger" role="alert">
                                    Il prodotto è stato modificato ma l'immagine non è stata salvata.
                                </div>
                            <?php } ?>
                            <form action="processProductUpdate.php" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="productID" value="<?php echo $_GET['productID'] ?>" />
                                <div class="form-group">
                                    <label for="productName">Nome prodotto</label>
                                    <input id="productName" class="form-control" type="text" name="name" value="<?php echo $product['Name'] ?>" required/>
                                </div>
                                <div class="form-group">
                                    <label for="productDesc">Descrizione prodotto</label>
                                    <textarea id="productDesc" class="form-control" type="text" name="description" rows="5" required><?php echo $product['Description'] ?></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="productPrice">Prezzo</label>
                                    <input id="productPrice" class="form-control" type="number" name="price" value="<?php echo $product['Price'] ?>" step="any" required>
                                </div>
                                <?php if (getCategories($categories, $mysqli) and count($categories) > 0) { ?>
                                    <div class="form-group">
                                        <label for="productCategory">Categoria</label>
                                        <select id="productCategory" class="form-control" name="category" required>
                                            <?php foreach($categories as $category) { ?>
                                                <?php if($categories['CategoryID'] == $product['CategoryID']) { ?>
                                                    <option value="<?php echo $category['CategoryID']?>" selected><?php echo $category['CategoryName']?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $category['CategoryID']?>"><?php echo $category['CategoryName']?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <?php } else { ?>
                                    <div class="alert alert-danger" role="alert">
                                        Non è stato possibile reperire le categorie.
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <label for="imageFile">Modifica immagine del prodotto</label>
                                    <div class="custom-file">
                                        <input id="imageFile" type="file" class="custom-file-input" name="imageSrc">
                                        <label class="custom-file-label" for="imageFile">Scegli un file...</label>
                                    </div>
                                </div>
                                <input class="btn btn-primary" type="submit" value="Salva modifiche"/>
                                <a class="btn btn-secondary" href="<?php echo "supplier.php?ID=".$product['SupplierID']?>">Annulla</a>
                            </form>
                        <?php } else { ?>
                            <div class="alert alert-danger" role="alert">
                                Non è stato possibile reperire il prodotto
                            </div>
                        <?php } ?>
                    </section>
                <?php } ?>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
