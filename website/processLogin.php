<?php
include 'dbUtils.php';
include 'secureSession.php';
sec_session_start();
if(isset($_POST['username'], $_POST['p'])) {
    $username = $_POST['username'];
    $password = $_POST['p'];
    if(login($username, $password, $mysqli) == true) {
        header('Location: '.getLoginCallbackPage());
    } else {
        header('Location: login.php?error=1');
    }
} else {
    header('Location: login.php?error=2');
}
?>
