<?php
include "dbUtils.php";
include "secureSession.php";
sec_session_start();
login_check($mysqli);
include "errorChecker.php";

if (userLoggedIn()) {
    header("Location: home.php");
    exit;
}
?>

<!DOCTYPE html>
<html lang="it">
    <head>
        <?php include "mainInclusions.php" ?>
        <script type="text/javascript" src="scripts/sha512.js"></script>
        <script type="text/javascript" src="scripts/forms.js"></script>
        <script type="text/javascript" src="scripts/login.js"></script>
        <title>Login - UniEat</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <?php if ($errNo == 2) { ?>
                    <div class="alert alert-danger" role="alert">
                        Mancano alcuni parametri.
                    </div>
                <?php } else if ($errNo == 1) { ?>
                    <div class="alert alert-danger" role="alert">
                        Impossibile eseguire il login.
                    </div>
                <?php } ?>
                <form action="processLogin.php" method="post" id="login-form">
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input class="form-control" type="text" name="username" id="username" required/>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input class="form-control" type="password" id="password" required/>
                    </div>
                    <input class="btn btn-primary" type="submit" value="Login" id="login-submit"/>
                </form>
                <p>
                    Non sei un membro?
                    <br/>
                    <a href="customerRegistration.php">Registrati</a>
                    <br/>
                    <a href="supplierRegistration.php">Registrati come fornitore</a>
                </p>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
