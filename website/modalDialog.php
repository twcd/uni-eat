<div class="modal fade" id="modaldialog" tabindex="-1" role="dialog" aria-labelledby="popUp" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="modal-title"></div>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btnCancel btn btn-secondary" data-dismiss="modal"></button>
                <button type="button" class="btnConfirm btn btn-primary" data-dismiss="modal"></button>
            </div>
        </div>
    </div>
</div>
