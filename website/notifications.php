<?php
include "dbUtils.php";
include "secureSession.php";
sec_session_start();
login_check($mysqli);
$rowsPerPage = 10;
$result = countNotifications(getUserID(), $notifications, $mysqli);
$pages = ceil($notifications['NumberOfNotifications']/$rowsPerPage);
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <script type="text/javascript" src="scripts/notifications.js"></script>
        <meta name="page-size" content="<?php echo $rowsPerPage ?>">
        <title>Notifications - UniEat</title>
    </head>
    <body>
        <?php include "navbar.php" ?>

        <div class="main-container">
            <div class="content">
                <section>
                    <h1>Notifiche</h1>
                    <nav aria-label="Notifications navigation">
                        <ul class="pagination justify-content-center mb-3">
                            <?php if($result){ ?>
                                <li class="page-item disabled"><a class="prev page-link" href="#" tabindex="-1"><<</a></li>
                                <?php for ($i=0; $i < $pages; $i++){ ?>
                                    <li class="onePage page-item">
                                        <a class="page-link <?php echo 'pageNum'.$i?>" href="#"><?php echo ($i + 1) ?></a>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                            <li class="page-item"><a class="next page-link" href="#">>></a></li>
                        </ul>
                    </nav>
                    <div class="notificationsList">

                    </div>
                    <nav aria-label="Notifications navigation">
                        <ul class="pagination justify-content-center mb-0 mt-3">
                            <?php if($result){ ?>
                                <li class="page-item disabled"><a class="prev page-link" href="#" tabindex="-1"><<</a></li>
                                <?php for ($i=0; $i < $pages; $i++){ ?>
                                    <li class="page-item">
                                        <a class="page-link <?php echo 'pageNum'.$i?>" href="#"><?php echo ($i + 1) ?></a>
                                    </li>
                                <?php } ?>
                                <li class="page-item"><a class="next page-link" href="#">>></a></li>
                            <?php } ?>
                        </ul>
                    </nav>
                </section>
            </div>

        </div>
        <?php include "footer.php" ?>
    </body>
</html>
