<?php
include "dbUtils.php";
include "secureSession.php";
include "fileSystemUtils.php";

sec_session_start();

$username = $_POST['username'];
$password = $_POST['p'];
$email = $_POST['email'];
$phoneNumber = $_POST['phone'];
$name = $_POST['name'];
$short = $_POST['shortDesc'];
$long = $_POST['longDesc'];
$address = $_POST['address'];

if (!isset($username, $password, $email, $phoneNumber, $name, $short, $long, $address)) {
    header('Location: supplierRegistration.php?error=1');
    exit;
}
if (!registerSupplier($username, $password, $name, $short, $long, $address, $email, $phoneNumber, $supplierID, $mysqli)) {
    header('Location: supplierRegistration.php?error=2');
    exit;
}
if (!saveSupplierImage($supplierID, $_FILES['imageSrc'], '')) {
    header('Location: supplier.php?error=1');
    exit;
}
if (!login($username, $password, $mysqli)) {
    header('Location: login.php?error=1');
    exit;
}

header('Location: supplier.php');
?>
