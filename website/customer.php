<?php
include "dbUtils.php";
include "secureSession.php";

sec_session_start();
login_check($mysqli);

if (userLoggedIn()) {
    $userID = getUserID();
} else {
    header("Location: home.php");
    exit;
}
if (!getCustomerData($userID, $username, $email, $name, $surname, $phone, $mysqli)) {
    $errorMessage = "Il cliente con ID $userID non è stato trovato. Forse hai modificato manualmente l'URL?";
}
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <title><?php echo $username ?> - UniEat</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <?php if (isset($errorMessage)) { ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $errorMessage ?>
                    </div>
                <?php } else {?>
                    <section class="text-center">
                        <h1><?php echo $username ?></h1>
                        <h2 class="border-bottom">Informazioni</h2>
                        <div class="container-fluid mb-3">
                            <div class="row">
                                <div class="col-sm-6 text-sm-right">
                                    <strong>Nome</strong>
                                </div>
                                <div class="col-sm-6 text-sm-left">
                                    <?php echo $name ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 text-sm-right">
                                    <strong>Cognome</strong>
                                </div>
                                <div class="col-sm-6 text-sm-left">
                                    <?php echo $surname ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 text-sm-right">
                                    <strong>Email</strong>
                                </div>
                                <div class="col-sm-6 text-sm-left">
                                    <?php echo $email ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 text-sm-right">
                                    <strong>Telefono</strong>
                                </div>
                                <div class="col-sm-6 text-sm-left">
                                    <?php echo $phone ?>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-4 mb-2 mb-md-0">
                                    <a class="btn btn-primary btn-block" href="ordersListPage.php">I miei ordini</a>
                                </div>
                                <div class="col-md-4 mb-2 mb-md-0">
                                    <a class="btn btn-primary btn-block" href="cart.php">Il mio carrello</a>
                                </div>
                                <div class="col-md-4">
                                    <a class="btn btn-primary btn-block" href="customerManageProfile.php".>Aggiorna il profilo</a>
                                </div>
                            </div>
                        </div>
                    </section>
                <?php } ?>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
