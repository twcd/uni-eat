function showModal(id){
    $.ajax({
        url: "api/requestProduct.php",
        data: { id:id },
        success: function (data) {
            console.log(data);
            if(data.error != 0){
                console.log("error number: " + data.error);
                return;
            }
            console.log(data['product']);
            $('.productTitle').html("Informazioni prodotto");
            $('.productImg').attr({
                src: data.product.imageSrc,
                alt: 'Immagine di ' + data.product.Name
            });
            $('.productDesctiption').html(data.product.Description);
            $('.productName').html(data.product.Name);
            $('.productPrice').html(money(data.product.Price));
            $('#toCart').attr("href", "addToCart.php?id="+id);
            $('#productPopUp').modal('show');
        }
    });
}
