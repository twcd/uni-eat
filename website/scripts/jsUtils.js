function money (value){
    return "€ "+value.toFixed(2).replace(".", ",");
}

function parseMoney(string) {
    return Number(string.trim().substring(2).replace(",", "."));
}

function stateToString(state){
    switch (state) {
        case "Cart":
            return "In carrello";
        case "Sent":
            return "In attesa di approvazione";
        case "Accepted":
            return "Accettato";
        case "Delivering":
            return "In consegna";
        case "Delivered":
            return "Consegnato";
        case "Declined":
                return "Rifiutato";
        case "Invalid":
            return "Non valido";
        default:
            return "SCONOSCIUTO";
    }
}

function setOrderState(id, newStatus, callback) {
    $.ajax({
        url: 'api/changeOrderState.php',
        type: 'POST',
        data:{ id: id, state: newStatus},
        success: callback
    });
}

function setOrderButtons(order, status){
    order.find('.orderSent').attr('disabled', status != "Accepted");
    order.find('.orderDelivered').attr('disabled', status != "Delivering");
    order.find('.declineOrder').attr('disabled', status != "Sent");
    order.find('.acceptOrder').attr('disabled', status != "Sent");
}

function getNotificationData(n) {
    let href;
    let text;
    switch (n.NotificationType) {
        case "OrderAccepted":
            href = 'order.php?id=' + n.OrderID;
            text = n.ShopName + ': il tuo ordine è stato accettato.';
            break;
        case "OrderReceived":
            href = 'order.php?id=' + n.OrderID;
            text = 'Hai ricevuto un nuovo ordine. Clicca qui per i dettagli';
            break;
        case "OrderDelivered":
            href = 'order.php?id=' + n.OrderID;
            text = 'Il tuo ordine a ' + n.ShopName + ' è stato consegnato.';
            break;
        case "OrderInvalidated":
            href = 'order.php?id=' + n.OrderID;
            text = 'Il tuo ordine a ' + n.ShopName + ' è stato invalidato perchè uno dei prodotti richiesti non è più disponibile.';
            break;
        case "OrderDeclined":
            href = 'order.php?id=' + n.OrderID;
            text = 'Il tuo ordine a ' + n.ShopName + ' è stato rifiutato.';
            break;
        case "OrderDelivering":
            href = 'order.php?id=' + n.OrderID;
            text = 'Il tuo ordine a ' + n.ShopName + ' è stato spedito.';
            break;
        case "NewSupplier":
            href = 'admin.php';
            text = 'Un nuovo fornitore si è appena registrato.';
            break;
        case "AccountApproved":
            href = 'supplier.php';
            text = 'Il tuo account è stato approvato. Da adesso in poi sarai visibile ai clienti.';
            break;
        case "AccountDisabled":
            href = 'supplier.php';
            text = 'Il tuo account è stato disabilitato da un amministratore.';
            break;
        default:
            href = '#';
            text = 'CASO DEFAULT: ' + n.NotificationID + ' ' + n.ShopName + ' ' + n.OrderID + ' ' + n.NotificationType + ' ' + n.NotificationDateTime;
            break;
    }
    return {
        href,
        text
    };
}
