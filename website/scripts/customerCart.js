let total;

function updateQuantityInDom(line, quantity) {
    line.find('.btn-minus').attr({ disabled: (quantity <= 1) });
    line.find('.item-quantity').text(quantity);
}

function updateTotal(increment) {
    total += increment;
    $('.total-display').text(money(total));
}

function changeQuantity(line, id, delta) {
    let currentQuantity = Number(line.find('.item-quantity').text());
    let newQuantity = currentQuantity + delta;
    let price = parseMoney(line.find('.item-price').text());
    $.ajax({
        url: "api/updateCartItemQuantity.php?id=" + id + "&quantity=" + newQuantity,
        success: function(data) {
            if (data.error) {
                console.log("Error requesting to update quantity (errno: " + data.error + ")");
                return;
            }
            updateQuantityInDom(line, newQuantity);
            updateTotal(price * delta);
        }
    });
}

function removeItemInDom(line) {
    line.slideUp(200,function(){
        line.remove();
    });
    let price = parseMoney(line.find('.item-price').text());
    let currentQuantity = Number(line.find('.item-quantity').text());
    updateTotal(-price * currentQuantity);
}

function removeItem(line, id) {
    $.ajax({
        url: "api/removeFromCart.php?id=" + id,
        success: function(data) {
            if (data.error) {
                console.log("Error requesting to remove from cart (errno: " + data.error + ")");
                return;
            }
            removeItemInDom(line);
        }
    });
}

$(document).ready(function() {
    total = parseMoney($('#cartTotal').text());
    $('.lineRow').each(function() {
        let line = $(this);
        let myID = line.find('.reqID').text();
        let myName = line.find('.productName').text().trim();
        line.find('.btn-plus').click(function(e) {
            changeQuantity(line, myID, 1);
        });
        line.find('.btn-minus').click(function(e) {
            changeQuantity(line, myID, -1);
        });
        updateQuantityInDom(line, Number(line.find('.item-quantity').text()));
        line.find('.btnRemove').click(function(e) {
            showDialog({
                message: "Confermi di voler rimuovere " + myName + " dal carrello?",
                onConfirmed: function() {
                    removeItem(line, myID);
                }
            });
        });
    });
});
