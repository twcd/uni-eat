let newState;
let id;
$(document).ready(function(){
    let lines = $('.lineRow');
    for (let i = 0; i < lines.length; i++) {
        let line = lines.eq(i);
        line.find('.btn').click(function(){
            id = line.children('.supplierID').html();
            if($(this).hasClass("btnApprove")){
                newState = 1;
            }
            if($(this).hasClass("btnRemove")){
                newState = 0;
            }
            $.ajax({
                url: 'api/setSupplierState.php',
                type: 'POST',
                data: { id:id, state:newState },
                success: function(response){
                    console.log(response);
                    if(response == 0){
                        if(newState == 0){
                            line.find('.btnRemove').addClass("d-none");
                            line.find('.btnApprove').removeClass("d-none")
                        }
                        if(newState == 1){
                            line.find('.btnRemove').removeClass("d-none");
                            line.find('.btnApprove').addClass("d-none");
                        }
                    }

                }
            });
        });
    }
});
