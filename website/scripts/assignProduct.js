$(document).ready(function(){
    let lines = $('.lineRow');
    for (let i = 0; i < lines.length; i++) {
        let line = lines.eq(i);
        line.find('.imgThumbnail').click(function(e){
            e.preventDefault();
            let pid = line.children('.productID').html();
            showModal(pid);
        });
    }
});
