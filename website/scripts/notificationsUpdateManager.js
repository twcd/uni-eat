const timer = 5000;
const count = 5;
let notifications = [];
let currentMaxID = 0;
let notViewedCount = 0;
let changed = true;

function addNotifications(ns) {
    notifications = ns.concat(notifications).slice(0, count);
    currentMaxID = Math.max(...(notifications.map(n => n.NotificationID)));
}

function createNotificationPreview(n) {
    let { text, href } = getNotificationData(n);
    let obj = $('<a>').attr({
        href: href
    })
    .addClass('dropdown-item text-wrap border-bottom')
    .text(text)
    .click(() => viewNotification(n));
    if (!n.Viewed) {
        obj.addClass('bg-secondary text-white');
    }
    return obj;
}

function updateCountInDom() {
    if (notViewedCount > 0) {
        $('.notification-count').show();
        $('.notification-count').html(notViewedCount);
    } else {
        $('.notification-count').hide();
    }
}

function viewNotification(n) {
    if (n.Viewed) {
        return;
    }
    console.log("Viewing notification: " + n.NotificationID);
    n.Viewed = true;
    notViewedCount--;
    updateCountInDom();
    $.ajax({
        url: "api/viewNotification.php?id=" + n.NotificationID,
        success: function(data) {
            if (data.error) {
                console.log("Error requesting to view a notification (errno: " + data.error + ")");
            }
        }
    });
}

function updateDocument() {
    updateCountInDom();
    if (changed) {
        if (notifications.length > 0) {
            $('.notifications-present').show();
            $('.notifications-absent').hide();
        } else {
            $('.notifications-present').hide();
            $('.notifications-absent').show();
        }
        let container = $('.notifications-container');
        container.empty();
        for (let n of notifications.map(createNotificationPreview)) {
            container.append(n);
        }
        changed = false;
    }
}

function doNotificationRequest(url) {
    $.ajax({
        url,
        success: function(data) {
            if (data.error != 0) {
                console.log("There was an error loading notifications (errno: " + data.error + ")");
                return;
            }
            if (data.notifications.length > 0) {
                changed = true;
            }
            addNotifications(data.notifications);
            notViewedCount = data.count;
            //sessionStorage.setItem('notifications', notifications);
            updateDocument();
        },
        complete: function() {
            setTimeout(refreshNotifications, timer);
        }
    });
}

function refreshNotifications() {
    doNotificationRequest("api/notifications.php?count=" + count + "&afterid=" + currentMaxID);
}

$(document).ready(function() {
    //let stored = sessionStorage.getItem('notifications');
    //if (stored) {
    //    addNotifications(stored);
    //}
    doNotificationRequest("api/notifications.php?count=" + count);
});
