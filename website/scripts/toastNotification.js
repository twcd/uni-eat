function showToast(options) {
    let toast = createToast(options);
    $('.toast-container').append(toast);
    toast.toast('show');
    toast.on('hidden.bs.toast', function () {
        toast.remove();
    });
}

function createToast(options){
    let toast = $(`<div role="status" aria-live="polite" aria-atomic="true" id="myToast" class="toast mw-100" data-delay="${options.delay}">
                        <div class="toast-header">
                            <span class="text-large fas fa-exclamation-circle fa-fw"></span>
                            <strong class="mr-auto pl-1">${options.title}</strong>
                            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="toast-body">
                        ${options.message}
                        </div>
                    </div>`);
    return toast;
}
