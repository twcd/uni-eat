let showModal = true;

function getNextAvailableDate(offset) {
    return new Date(Date.now() + (30 + offset) * 60 * 1000);
}

function checkDateAndTime(date) {
    return date > getNextAvailableDate(-1);
}

function onCorrectFormSubmit(form) {
    showDialog({
        title: "Conferma",
        message: "Confermi di voler procedere con l'ordine?",
        onConfirmed: function() {
            showModal = false;
            form.submit();
        }
    });
}

function onFormSubmit(form) {
    event.preventDefault();
    let date = form.find('input[type="date"]');
    let time = form.find('input[type="time"]');
    let reqDate = new Date(date.val() + ' ' + time.val());
    if (!checkDateAndTime(reqDate)) {
        showDialog({
            title: 'Orario errato',
            message: 'Impossibile effettuare la richiesta a questo orario. Impostare il primo orario disponibile?',
            confirmText: 'Ok',
            onConfirmed: function() {
                showModal = false;
                reqDate = getNextAvailableDate(0);
                time.val(reqDate.toTimeString());
                form.submit();
            }
        });
        return;
    }
    onCorrectFormSubmit(form);
}

$(document).ready(function() {
    let form = $('#orderform');
    form.find('input[type="time"]').val(getNextAvailableDate(5).toTimeString().substring(0, 5));
    form.submit(function(event) {
        if (!showModal) {
            showModal = true;
            return;
        }
        onFormSubmit($(this));
    });
});
