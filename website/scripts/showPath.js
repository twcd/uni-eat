$(document).ready(function(){
    $('.custom-file input').change(function(e){
        if(e.target.files.length > 0){
            let filesArray = [];
            for (let i = 0; i < e.target.files.length; i++) {
                filesArray.push(e.target.files[i]);
            }
            let fileName = filesArray.map(p => p.name).join(", ");
            console.log(fileName);
            $('.custom-file label').text(fileName);
        } else {
            $('.custom-file label').text("Scegli un file...");
        }

    })
});
