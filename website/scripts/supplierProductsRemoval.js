let id;

$(document).ready(function(){
    let lines = $('.lineRow');

    for (let i = 0; i < lines.length; i++) {
        let line = lines.eq(i);
        line.find('.btnRemove').click(function(){
            id = line.children('.productID').html();
            showDialog({
                message: "Confermi di voler rimuovere questo prodotto dal listino?",
                onConfirmed: function() {
                    $.ajax({
                        url: 'api/productRemoval.php',
                        type: 'POST',
                        data: { id:id },
                        success: function(response){
                            console.log(response);
                            if(response == 0){
                                setTimeout(() =>{
                                    line.slideUp(500,function(){
                                        line.remove();
                                    });
                                    showToast({
                                        delay: 3000,
                                        title: "Rimozione",
                                        message: "Prodotto rimosso dal listino con successo"
                                    });
                                }, 500);
                            }else if (response == 1){
                                showToast({
                                    delay: 3000,
                                    title: "Rimozione",
                                    message: "Impossibile rimuovere il prodotto desiderato"
                                });
                            }else if (response == -1){
                                showToast({
                                    delay: 3000,
                                    title: "Rimozione",
                                    message: "Il prodotto non è più presente"
                                });
                            }
                        }
                    });
                }
            });
        });
    }
});
