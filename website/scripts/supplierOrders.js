$(document).ready(function(){
    let ordersList = $('.orderLine');
    for (let i = 0; i < ordersList.length; i++) {
        let order = ordersList.eq(i);
        let status = order.find('.status').text();
        let orderID = order.find('.orderID').text();
        updateOrderLine(order, status);
        order.find('.acceptOrder').click(function(){
            setOrderState(orderID, "Accepted", () => updateOrderLine(order, "Accepted"));
        });
        order.find('.declineOrder').click(function(){
            setOrderState(orderID, "Declined", () => updateOrderLine(order, "Declined"));
        });
        order.find('.orderSent').click(function(){
            setOrderState(orderID, "Delivering", () => updateOrderLine(order, "Delivering"));
        });
        order.find('.orderDelivered').click(function(){
            setOrderState(orderID, "Delivered", () => updateOrderLine(order, "Delivered"));
        });
    }
});

function updateOrderLine(order, status) {
    let id = order.find('.orderID').text();
    order.find('.orderState').removeClass("text-danger text-warning text-success text-info text-secondary");
    switch (status) {
        case "Sent":
            order.find('.orderState').addClass('text-warning');
            break;
        case "Accepted":
            order.find('.orderState').addClass('text-info');
            break;
        case "Declined":
        case "Invalid":
            order.find('.orderState').addClass('text-danger');
            break;
        case "Delivered":
            order.find('.orderState').addClass('text-success');
            break;
        case "Delivering":
            order.find('.orderState').addClass('text-secondary');
            break;
    }

    order.find('.orderState').text(stateToString(status));
    order.find('.orderSent').attr('disabled', status != "Accepted");
    order.find('.orderDelivered').attr('disabled', status != "Delivering");
    order.find('.declineOrder').attr('disabled', status != "Sent");
    order.find('.acceptOrder').attr('disabled', status != "Sent");
    setOrderButtons(order, status);
}
