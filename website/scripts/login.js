$(document).ready(function() {
    $('input#login-submit').click(function(event) {
        const username = $('input#username').val();
        const password = $('input#password').val();
        if (username == "" || password == "") {
            event.preventDefault();
        } else {
            formhash($('form#login-form'), password);
        }
    });
});
