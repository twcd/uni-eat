let modalObject;
let cancelCallback;
let confirmCollback;

function showDialog(options) {
    let header = modalObject.find('.modal-header');
    let title = modalObject.find('.modal-title');
    let body = modalObject.find('.modal-body');

    if (options.title) {
        header.show();
        header.find('.modal-title').text(options.title);
    } else {
        header.hide();
    }

    body.text(options.message);

    cancelCallback = options.onCanceled;
    confirmCollback = options.onConfirmed;
    modalObject.find('.btnCancel').text(options.cancelText ? options.cancelText : "Annulla");
    modalObject.find('.btnConfirm').text(options.confirmText ? options.confirmText : "Conferma");
    modalObject.modal('show');
}

$(document).ready(function() {
    modalObject = $('#modaldialog');
    modalObject.find('.btnCancel').click(function(e) {
        if (cancelCallback) {
            cancelCallback(e);
        }
    });
    modalObject.find('.btnConfirm').click(function(e) {
        if (confirmCollback) {
            confirmCollback(e);
        }
    });
});
