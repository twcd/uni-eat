const minLength = 8;

function setNeutral(input, feedback){
    input.removeClass("is-invalid");
    input.removeClass("is-valid");
    feedback.removeClass("invalid-feedback");
    feedback.removeClass("valid-feedback");
    feedback.text("");
}

function setValid(input, feedback, text){
    input.removeClass("is-invalid");
    input.addClass("is-valid");
    feedback.removeClass("invalid-feedback");
    feedback.addClass("valid-feedback");
    feedback.text(text);
}
function setInvalid(input, feedback, text){
    input.removeClass("is-valid");
    input.addClass("is-invalid");
    feedback.removeClass("valid-feedback");
    feedback.addClass("invalid-feedback");
    feedback.text(text);
}

function checkPassword(input, feedback) {
    if(input.val().length < minLength){
        setInvalid(input, feedback, "Lunghezza minima: 8 caratteri");
        return false;
    }
    setValid(input, feedback, "Password corretta");
    return true;
}

function checkEquality(input1, input2, feedback) {
    if (input1.val() != input2.val()) {
        setInvalid(input2, feedback, "Le password NON corrispondono");
        return false;
    }
    return true;
}

$(document).ready(function() {
    let myInput = $("#psw1");
    let myInput2 = $("#psw2");
    let feedback1 = $("#psw1Feedback");
    let feedback2 = $("#psw2Feedback");
    myInput.on("input", () => checkPassword(myInput, feedback1));

    myInput2.on("input", () => setNeutral(myInput2, feedback2));

    $('form#register-form').submit(function(event) {
        if (!checkPassword(myInput, feedback1) || !checkEquality(myInput, myInput2, feedback2)) {
            event.preventDefault();
        } else {
            formhash($('form#register-form'), myInput.val());
        }
    });
});
