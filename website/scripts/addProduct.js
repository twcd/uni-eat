$(document).ready(function(){
    $("form#data").submit(function(e) {
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            type: 'POST',
            url: "api/addProduct.php",
            data: formData,
            success: function (response) {
                if(response.error == 0){
                    $('#productName').val("");
                    $('#productDesc').val("");
                    $('#productPrice').val("");
                    $('.custom-file-label').text("Scegli un file...");
                    showToast({
                        delay: 3000,
                        title: "Nuova Notifica: Aggiunta",
                        message: "Prodotto aggiunto al listino con successo"
                    });
                }
                if(response.error == 1){
                    console.log("err1");
                }
                if(response.error == 2){
                    console.log("err2");
                }
            },
            cache: false,
            contentType: false,
            processData: false
        });
    });
});
