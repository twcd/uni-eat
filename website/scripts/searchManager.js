$(document).ready(function() {
    $('#products .lineRow').each(function() {
        let id = Number($(this).find('.productID').text());
        $(this).find('.imgThumbnail').click(function(e) {
            e.preventDefault();
            showModal(id);
        })
    });

    $('#suppliers-tab').click(function() {
        $('input[name="t"]').val('suppliers');
    });
    
    $('#products-tab').click(function() {
        $('input[name="t"]').val('products');
    });
});
