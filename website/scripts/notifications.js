let page = 0;
let num_pages;
let nPerPage;

$(document).ready(function(){
    num_pages = $('.onePage').length;
    nPerPage = Number($('meta[name="page-size"]').attr("content"));
    for (let i = 0; i < num_pages; i++) {
        let myIndex = i;
        $('.pageNum'+i).click(function() {
            page = myIndex;
            requestPage(page);
        });
    }
    $('.prev').click(function() {
        page--;
        requestPage(page);
    });
    $('.next').click(function() {
        page++;
        requestPage(page);
    });
    requestPage(0);
});

function requestPage(index) {
    $.ajax({
        url: 'api/notifications.php',
        data: { page: index, count: nPerPage},
        success: function(result){
            deleteRows();
            if(result.error == 0){
                for (let n of result.notifications){
                    let { text, href } = getNotificationData(n);
                    $('.notificationsList').append(
                        `<div class="row border border-info rounded-notification py-1 mx-0 my-1">
                            <div class="col-md-4 text-center text-md-right px-2">
                                ${n.NotificationDateTime}
                            </div>
                            <div class="col-md-8 text-center text-md-left px-2">
                                <a href="${href}">${text}</a>
                            </div>
                        </div>`);
                }
            }
            else{
                $('.notificationsList').append('<div class="alert alert-danger" role="alert">Non è stato possibile caricare le notifiche</div>');
            }
        }
    });
    manage_pages();
}

function deleteRows(){
    $( ".row" ).remove();
}

function manage_pages() {
    $(".pagination li").removeClass("disabled active");

    if (page == 0) {
        $(".prev").parent().addClass("disabled");
    }

    if (page == num_pages - 1) {
        $(".next").parent().addClass("disabled");
    }

    $(".pageNum" + page).parent().addClass("active");
}
