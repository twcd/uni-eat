$(document).ready(function(){
    let order = $('.content');

    let status = order.find('.status').text();
    let orderID = order.find('.orderID').text();
    setOrderButtons(order, status);
    order.find('.acceptOrder').click(function(){
        setOrderState(orderID, "Accepted", () => setOrderButtons(order, "Accepted"));
        order.find('.actualState').text(stateToString("Accepted"));
    });
    order.find('.declineOrder').click(function(){
        setOrderState(orderID, "Declined", () => setOrderButtons(order, "Declined"));
        order.find('.actualState').text(stateToString("Declined"));
    });
    order.find('.orderSent').click(function(){
        setOrderState(orderID, "Delivering", () => setOrderButtons(order, "Delivering"));
        order.find('.actualState').text(stateToString("Delivering"));
    });
    order.find('.orderDelivered').click(function(){
        setOrderState(orderID, "Delivered", () => setOrderButtons(order, "Delivered"));
        order.find('.actualState').text(stateToString("Delivered"));
    });

});
