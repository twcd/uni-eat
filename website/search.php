<?php
include "dbUtils.php";
include "secureSession.php";
include "fileSystemUtils.php";
include "misc.php";
sec_session_start();
login_check($mysqli);

if (isset($_GET['q'])) {
    $queryResult = getSearchResults($_GET['q'], $results, $mysqli);
}

if (isset($_GET['t'])) {
    $activeType = $_GET['t'];
} else {
    $activeType = 'suppliers';
}
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <script type="text/javascript" src="scripts/searchManager.js"></script>
        <script type="text/javascript" src="scripts/showProduct.js"></script>
        <title>Cerca - UniEat</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <section>
                    <h1>Cerca</h1>
                    <div class="mb-3">
                        <form action="search.php" method="get" class="search-form">
                            <input type="hidden" name="t" value="<?php echo $activeType ?>">
                            <div class="input-group">
                                <input type="text" id="mainSearchBar" name="q" class="form-control" placeholder="Cerca in UniEat" aria-label="Cerca in UniEat" value="<?php echo isset($_GET['q']) ? $_GET['q'] : "" ?>" required/>
                                <div class="input-group-append">
                                    <button class="btn btn-secondary" type="submit"><span class="fas fa-search"></span> Cerca</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <?php if (!$queryResult) { ?>
                        <div class="alert alert-danger" role="alert">
                            Impossibile ottenere i risultati della ricerca.
                        </div>
                    <?php } else { ?>
                        <div>
                            <ul class="nav nav-tabs" id="search-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link <?php echo $activeType == 'suppliers' ? "active" : "" ?>" id="suppliers-tab" data-toggle="tab" href="#suppliers" role="tab" aria-controls="suppliers" aria-selected="true">Fornitori (<?php echo count($results['suppliers']) ?>)</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link <?php echo $activeType == 'suppliers' ? "" : "active" ?>" id="products-tab" data-toggle="tab" href="#products" role="tab" aria-controls="products" aria-selected="false">Prodotti (<?php echo count($results['products']) ?>)</a>
                                </li>
                            </ul>
                            <div class="tab-content pt-2" id="searchResults">
                                <div class="tab-pane fade show active" id="suppliers" role="tabpanel" aria-labelledby="suppliers-tab">
                                    <?php if (count($results['suppliers']) == 0) { ?>
                                        <div class="alert alert-warning" role="alert">
                                            Nessun fornitore corrispondente ai parametri di ricerca.
                                        </div>
                                    <?php } else { ?>
                                        <?php foreach ($results['suppliers'] as $supplier) { ?>
                                            <div class="lineRow d-flex p-2 p-md-3 flex-row">
                                                <div class="col-2 p-0">
                                                    <div class="d-flex flex-column justify-content-center h-100">
                                                        <a class="thumbnail" href="<?php echo "supplier.php?id=".$supplier['UserID'] ?>">
                                                            <img class="center-cropped rounded-thumbnail" src="<?php echo getSupplierImages($supplier['UserID'], "")[0] ?>" alt="Immagine del profilo di <?php echo $supplier['ShopName']?>"/>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-10 px-2">
                                                    <div class="d-flex flex-row align-items-center justify-content-between">
                                                        <div class="productName text-truncate">
                                                            <a class="h3" href="<?php echo "supplier.php?id=".$supplier['UserID'] ?>"><?php echo $supplier['ShopName']?></a>
                                                        </div>
                                                    </div>
                                                    <div class="w-100 text-truncate">
                                                        <?php echo $supplier['ShortDescription'] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <div class="tab-pane fade" id="products" role="tabpanel" aria-labelledby="products-tab">
                                    <?php if (count($results['products']) == 0) { ?>
                                        <div class="alert alert-warning" role="alert">
                                            Nessun prodotto corrispondente ai parametri di ricerca.
                                        </div>
                                    <?php } else { ?>
                                        <?php foreach ($results['products'] as $product) { ?>
                                            <div class="lineRow d-flex p-2 p-md-3 flex-row">
                                                <span hidden class="productID"><?php echo $product['ProductID'] ?></span>
                                                <div class="col-2 p-0">
                                                    <div class="d-flex flex-column justify-content-center h-100">
                                                        <div class="thumbnail">
                                                            <a href class="imgThumbnail w-100 h-100">
                                                                <img class="center-cropped rounded-thumbnail" src="<?php echo getProductImagePath($product['ProductID'], ''); ?>" alt="Immagine di <?php echo $product['Name']?>">
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="<?php echo (isCustomer() or !userLoggedIn()) ? "col-8" : "col-10" ?> px-2">
                                                    <div class="d-flex flex-row align-items-center justify-content-between">
                                                        <div class="text-truncate">
                                                            <strong><?php echo $product['Name']?></strong>
                                                        </div>
                                                        <span class="flex-shrink-0">
                                                            <?php echo money($product['Price'])?>
                                                        </span>
                                                    </div>
                                                    <div class="w-100 multi-line">
                                                        <?php echo $product['Description'] ?>
                                                    </div>
                                                </div>
                                                <?php if (isCustomer() or !userLoggedIn()){ ?>
                                                    <div class="col-2 p-0">
                                                        <div class="d-flex flex-column justify-content-start h-100">
                                                            <a class="btn btn-outline-secondary btn-sm" href="<?php echo "addToCart.php?id=".$product['ProductID']; ?>">
                                                                <span class="fas fa-cart-plus fa-fw"></span>
                                                                <span class="d-none d-md-block">Aggiungi al carrello</span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </section>
            </div>
        </div>
        <?php include "productPage.php" ?>
        <?php include "footer.php" ?>
    </body>
</html>
