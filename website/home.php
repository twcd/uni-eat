<?php
include "dbUtils.php";
include "secureSession.php";
include "fileSystemUtils.php";
include "misc.php";
sec_session_start();
login_check($mysqli);
$idx = 9;
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <link rel="stylesheet" href="stylesheets/mainSlideshow.css"/>
        <script type="text/javascript" src="scripts/showProduct.js"></script>
        <script type="text/javascript" src="scripts/multiCarousel.js"></script>
        <title>UniEat Home</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div id="shopSlideshow" class="carousel slide main-slideshow" data-interval="8000" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#shopSlideshow" data-slide-to="0" class="active"></li>
                    <li data-target="#shopSlideshow" data-slide-to="1"></li>
                    <li data-target="#shopSlideshow" data-slide-to="2"></li>
                </ol>
                <a class="carousel-control-prev" href="#shopSlideshow" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#shopSlideshow" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                <div class="carousel-inner h-100">
                    <div class="carousel-item h-100 active">
                        <img class="center-cropped" src="images/Piadina.jpg" alt="Una piadina farcita"/>
                        <div class="carousel-caption">
                            <div class="h4">
                                Vuoi entrare a far parte di UniEat?
                            </div>
                            <a href="customerRegistration.php" class="btn btn-primary mb-4">Registrati</a>
                        </div>
                    </div>
                    <div class="carousel-item h-100">
                        <img class="center-cropped" src="images/fattorino.jpg" alt="Un fattorino"/>
                        <div class="carousel-caption">
                            <div class="h4">
                                Visita i nostri fornitori
                            </div>
                            <a href="suppliersList.php" class="btn btn-primary mb-4">Tutti i fornitori</a>
                        </div>
                    </div>
                    <div class="carousel-item h-100">
                        <img class="center-cropped" src="images/locale.jpg" alt="Un locale"/>
                        <div class="carousel-caption">
                            <div class="h4">
                                Sei un fornitore in zona campus?
                            </div>
                            <a href="supplierRegistration.php" class="btn btn-primary mb-4">Diventa partner</a>
                        </div>
                    </div>
                </div>
            </div>

            <h1 class="sr-only">UniEat</h1>
            <img class="blackLogo mt-5" src="images/unieat-black.png" alt="UniEat Logo">
            <div class="content">
                <h2 class="border-bottom text-center mt-2">Prodotti più venduti</h2>
                <div id="productsSlideshow" class="carousel slide" data-interval="false">
                    <div class="carousel-inner h-100">
                        <?php if(getTopRankedProducts($products, $idx, $mysqli)){
                            for ($i = 0; $i < count($products); ) { ?>
                                <div class="carousel-item h-100">
                                    <div class="d-flex flex-row align-items-center h-100 px-4">
                                        <?php for ($j = 0; ($i < count($products) and $j < 3); $j++, $i++) { ?>
                                            <div class="slide-image col-4 p-1">
                                                <div class="card">
                                                    <div class="card-top">
                                                        <a href="" class="productModal">
                                                            <span hidden class="productID"><?php echo $products[$i]['ProductID'] ?></span>
                                                            <img class="card-img-top" src="<?php echo getProductImagePath($products[$i]['ProductID'], "") ?>" alt="Immagine di <?php echo $products[$i]['Name'] ?>">
                                                        </a>
                                                    </div>
                                                    <div class="card-body p-2">
                                                        <a class="text-truncate d-block card-title productModal" href="">
                                                            <?php echo $products[$i]['Name'] ?>
                                                            <span hidden class="productID"><?php echo $products[$i]['ProductID'] ?></span>
                                                        </a>
                                                        <?php echo money($products[$i]['Price']) ?>
                                                        <br/>
                                                        <?php echo $products[$i]['ShopName'] ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <a class="carousel-control-prev text-dark slideshw-navigation" href="#productsSlideshow" role="button" data-slide="prev">
                        <span class="fas fa-arrow-circle-left fa-2x" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next text-dark slideshw-navigation" href="#productsSlideshow" role="button" data-slide="next">
                        <span class="fas fa-arrow-circle-right fa-2x" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <h2 class="border-bottom text-center mt-5">I nostri partner</h2>
                <div id="suppliersSlideshow" class="carousel slide" data-interval="false">
                    <div class="carousel-inner h-100">
                        <?php if(getTopRankedSuppliers($suppliers, $idx, $mysqli)) {
                            for ($i = 0; $i < count($suppliers); ) { ?>
                                <div class="carousel-item h-100">
                                    <div class="d-flex flex-row align-items-center h-100 px-4">
                                        <?php for ($j = 0; ($i < count($suppliers) and $j < 3); $j++, $i++) { ?>
                                            <div class="slide-image col-4 p-1">
                                                <div class="card">
                                                    <div class="card-top">
                                                        <a href="<?php echo "supplier.php?id=".$suppliers[$i]['UserID'] ?>">
                                                            <img class="card-img-top" src="<?php echo getSupplierImages($suppliers[$i]['UserID'], "")[0] ?>" alt="Immagine del profilo di <?php echo $suppliers[$i]['ShopName'] ?>">
                                                        </a>
                                                    </div>
                                                    <div class="card-body p-2">
                                                        <a href="<?php echo "supplier.php?id=".$suppliers[$i]['UserID'] ?>" class="text-truncate d-block card-title"><?php echo $suppliers[$i]['ShopName'] ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                    <a class="carousel-control-prev text-dark slideshw-navigation" href="#suppliersSlideshow" role="button" data-slide="prev">
                        <span class="fas fa-arrow-circle-left fa-2x" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next text-dark slideshw-navigation" href="#suppliersSlideshow" role="button" data-slide="next">
                        <span class="fas fa-arrow-circle-right fa-2x" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        <?php include "productPage.php"?>
        <?php include "footer.php" ?>
    </body>
</html>
