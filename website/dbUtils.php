<?php
define("HOST", "localhost");
define("USER", "root");
define("PASSWORD", "");
define("DATABASE", "UniEatDB");

$mysqli = new mysqli(HOST, USER, PASSWORD, DATABASE);

function registerCustomer($username, $rawPassword, $name, $surname, $email, $phoneNumber, $mysqli) {
    $userID = createAccount($username, $rawPassword, $email, "customer", $mysqli);
    if ($userID < 0) {
        return false;
    }
    $stmt = $mysqli->prepare(
        "INSERT INTO Customers (UserID, Name, Surname, PhoneNumber)
        VALUES (?, ?, ?, ?)");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("isss", $userID, $name, $surname, $phoneNumber);
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->close();
    return true;
}

function registerSupplier($username, $rawPassword, $name, $shortDesc, $longDesc, $address, $email, $phoneNumber, &$userID, $mysqli) {
    $userID = createAccount($username, $rawPassword, $email, "supplier", $mysqli);
    if ($userID < 0) {
        return false;
    }
    $stmt = $mysqli->prepare(
        "INSERT INTO Suppliers (UserID, ShopName, ShortDescription, LongDescription, Address, PhoneNumber)
        VALUES (?, ?, ?, ?, ?, ?)");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("isssss", $userID, $name, $shortDesc, $longDesc, $address, $phoneNumber);
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->close();
    return true;
}

function createAccount($username, $rawPassword, $email, $type, $mysqli) {
    $realPassward = preprocessPassword($rawPassword, $salt);
    $stmt = $mysqli->prepare(
        "INSERT INTO UserAccounts (Username, PasswordHash, PasswordSalt, Email, UserType)
        VALUES (?, ?, ?, ?, ?)");
    if (!$stmt) {
        return -1;
    }
    $stmt->bind_param("sssss", $username, $realPassward, $salt, $email, $type);
    if (!$stmt->execute()) {
        return -1;
    }
    $stmt->close();
    return $mysqli->insert_id;
}

function getUserCompleteName($userID, $mysqli) {
    $stmt = $mysqli->prepare(
        "SELECT Name AS CompleteName
        FROM Customers
        WHERE UserID = ?
        UNION
        SELECT ShopName AS CompleteName
        FROM Suppliers
        WHERE UserID = ?
        UNION
        SELECT Username AS CompleteName
        FROM UserAccounts U INNER JOIN administrators A
        ON U.UserID = A.UserID
        AND A.UserID = ?");
    if (!$stmt) {
        return "Sconosciuto";
    }
    $stmt->bind_param("iii", $userID, $userID, $userID);
    if (!$stmt->execute()) {
        return "Sconosciuto";
    }
    $stmt->store_result();
    if ($stmt->num_rows != 1) {
        return "Sconosciuto";
    }
    $stmt->bind_result($name);
    $stmt->fetch();
    $stmt->close();
    return $name;
}

function getCustomerData($userID, &$username, &$email, &$name, &$surname, &$phone, $mysqli) {
    $stmt = $mysqli->prepare(
        "SELECT Username, Email, Name, Surname, PhoneNumber
        FROM UserAccounts A, Customers C
        WHERE A.UserID = ?
        AND C.UserID = A.UserID");
    if (!$stmt) {
        $username = "NOT FOUND";
        return false;
    }
    $stmt->bind_param("i", $userID);
    if (!$stmt->execute()) {
        $username = "NOT FOUND";
        return false;
    }
    $stmt->store_result();
    if ($stmt->num_rows != 1) {
        $username = "NOT FOUND";
        return false;
    }
    $stmt->bind_result($username, $email, $name, $surname, $phone);
    $stmt->fetch();
    $stmt->close();
    return true;
}

function getSupplierData($userID, &$username, &$email, &$name, &$shortDesc, &$longDesc, &$address, &$phone, $mysqli) {
    $stmt = $mysqli->prepare(
        "SELECT Username, Email, ShopName, ShortDescription, LongDescription, Address, PhoneNumber
        FROM UserAccounts A, Suppliers S
        WHERE A.UserID = ?
        AND S.UserID = A.UserID");
    if (!$stmt) {
        $username = "NOT FOUND";
        return false;
    }
    $stmt->bind_param("i", $userID);
    if (!$stmt->execute()) {
        $username = "NOT FOUND";
        return false;
    }
    $stmt->store_result();
    if ($stmt->num_rows != 1) {
        $username = "NOT FOUND";
        return false;
    }
    $stmt->bind_result($username, $email, $name, $shortDesc, $longDesc, $address, $phone);
    $stmt->fetch();
    $stmt->close();
    return true;
}

function getCart($userID, &$cart, $mysqli) {
    $cart = array();
    $query = "SELECT P.ProductID, P.Name, P.Price, R.Quantity, R.Notes, S.ShopName, R.ProductRequestID AS ReqID, S.UserID AS SupplierID, O.OrderID
        FROM Orders O, ProductRequests R, Products P, Suppliers S
        WHERE O.OrderID = R.OrderID
        AND R.ProductID = P.ProductID
        AND O.SupplierID = S.UserID
        AND O.State = 'Cart'
        AND O.CustomerID = ?";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("i", $userID);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $cart[] = $row;
    }
    $stmt->close();
    return true;
}

function getNotificationsCount($userID, &$count, $mysqli) {
    $count = 0;
    $stmt = $mysqli->prepare(
        "SELECT COUNT(*)
        FROM Notifications
        WHERE UserID = ?
        AND Viewed = 0");
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("i", $userID);
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->store_result();
    $stmt->bind_result($count);
    $stmt->fetch();
    $stmt->close();
    return true;
}

function countNotifications($userID, &$notifications, $mysqli){
    $stmt = $mysqli->prepare(
        "SELECT COUNT(NotificationID)
        AS NumberOfNotifications
        FROM Notifications
        WHERE UserID = ?");
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("i", $userID);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    $notifications = $result->fetch_assoc();
    $stmt->close();
    return true;
}

function getNotifications($userID, &$notifications, $pageNum, $rowsPerPage, $afterID, $mysqli) {
    $notifications = array();
    $query = "SELECT NotificationID, NotificationDateTime, NotificationType, Viewed, N.OrderID, ShopName
        FROM Notifications N LEFT OUTER JOIN (
            Orders O INNER JOIN Suppliers S
            ON O.SupplierID = S.UserID)
        ON N.OrderID = O.OrderID
        WHERE N.UserID = ?
        AND NotificationID > ?
        ORDER BY NotificationDateTime DESC
        LIMIT ?, ?";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $startRow = $pageNum * $rowsPerPage;
    $stmt->bind_param("iiii", $userID, $afterID, $startRow, $rowsPerPage);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $notifications[] = $row;
    }
    $stmt->close();
    return true;
}

function viewNotification($id, $mysqli) {
    $query = "UPDATE Notifications
        SET Viewed = 1
        WHERE NotificationID = ?";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $startRow = $pageNum * $rowsPerPage;
    $stmt->bind_param("i", $id);
    if (!$stmt->execute()) {
        return false;
    }
    return true;
}

function getCustomerOrders($userID, &$orders, $mysqli) {
    $orders = array();
    $query = "SELECT OrderID, State, DeliveryDateTime, RequestDateTime, ShopName, SupplierID, Location
        FROM Orders O, Suppliers S
        WHERE O.SupplierID = S.UserID
        AND STATE <> 'Cart'
        AND O.CustomerID = ?
        ORDER BY RequestDateTime DESC";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("i", $userID);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $orders[] = $row;
    }
    $stmt->close();
    return true;
}

function getSupplierOrders($userID, &$orders, $mysqli) {
    $orders = array();
    $query = "SELECT OrderID, RequestDateTime, DeliveryDateTime, Location, State
        FROM Orders
        WHERE SupplierID = ? AND State <> 'Cart'
        ORDER BY RequestDateTime DESC";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("i", $userID);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $orders[] = $row;
    }
    $stmt->close();
    return true;
}
function getSupplierProducts($userID, &$products, $mysqli) {
    $products = array();
    $query = "SELECT ProductID, Name, Description, Price
        FROM AvailableProducts P
        WHERE P.SupplierID = ?";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("i", $userID);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $products[] = $row;
    }
    $stmt->close();
    return true;
}
function registerProduct ($name, $description, $price, $supplierID, $categoryID, &$productID, $mysqli) {
    $stmt = $mysqli->prepare(
        "INSERT INTO Products (Name, Description, Price, SupplierID, CategoryID)
        VALUES (?, ?, ?, ?, ?)");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("ssdii",$name, $description, $price, $supplierID, $categoryID);
    if (!$stmt->execute()) {
        return false;
    }
    $productID = $mysqli->insert_id;
    $stmt->close();
    return true;
}

function getCategories(&$categories, $mysqli) {
    $categories = array();
    $query = "SELECT CategoryID, CategoryName
        FROM productcategories";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $categories[] = $row;
    }
    $stmt->close();
    return true;
}

function getSingleProduct($productID, &$product, $mysqli) {
    $query = "SELECT ProductID, Name, Description, Price, C.CategoryID, ShopName, P.SupplierID, CategoryName
        FROM Products P, Suppliers S, ProductCategories C
        WHERE P.ProductID = ?
        AND P.SupplierID = S.UserID
        AND P.CategoryID = C.CategoryID";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("i", $productID);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    $product = $result->fetch_assoc();
    $stmt->close();
    return true;
}

function updateProduct($name, $description, $price, $categoryID, $productID, $mysqli) {
    $stmt = $mysqli->prepare(
        "UPDATE Products
        SET Name = ?, Description = ?, Price = ?, CategoryID = ?
        WHERE ProductID = ?");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("ssdii", $name, $description, $price, $categoryID, $productID);
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->close();
    return true;
}

function createNewOrder($userID, $supplierID, &$orderID, $mysqli) {
    $stmt = $mysqli->prepare(
        "INSERT INTO Orders (CustomerID, SupplierID, State)
        VALUES (?, ?, 'Cart')");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("ii", $userID, $supplierID);
    if (!$stmt->execute()) {
        return false;
    }
    $orderID = $mysqli->insert_id;
    $stmt->close();
    return true;
}

function getOrderIDForProduct($userID, $productID, &$orderID, $mysqli) {
    if (!getCart($userID, $cart, $mysqli) or !getSingleProduct($productID, $product, $mysqli)) {
        return;
    }
    foreach ($cart as $cartItem) {
        if ($cartItem['SupplierID'] == $product['SupplierID']) {
            $orderID = $cartItem['OrderID'];
            return;
        }
    }
    createNewOrder($userID, $product['SupplierID'], $orderID, $mysqli);
}

function addProductToCart($userID, $productID, $quantity, $notes, $mysqli) {
    getOrderIDForProduct($userID, $productID, $orderID, $mysqli);
    if (!$orderID) {
        return false;
    }
    $stmt = $mysqli->prepare(
        "INSERT INTO ProductRequests
        (ProductID, OrderID, Quantity, Notes)
        VALUES
        (?, ?, ?, ?)");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("iiis", $productID, $orderID, $quantity, $notes);
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->close();
    return true;
}

function removeProduct($productID, $mysqli){
    $stmt = $mysqli->prepare("UPDATE Products
        SET Available = 0
        WHERE ProductID = ?");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("i",$productID);
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->close();
    return true;
}

function getOrderDetails($orderID, &$order, $mysqli) {
    $query = "SELECT O.*, S.ShopName, C.Name AS CustomerName,
        C.Surname AS CustomerSurname, C.PhoneNumber AS CustomerPhoneNumber
        FROM Orders O, Customers C, Suppliers S
        WHERE O.SupplierID = S.UserID
        AND O.CustomerID = C.UserID
        AND OrderID = ?";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("i", $orderID);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    $order = $result->fetch_assoc();
    $stmt->close();
    return true;
}

function getOrderRequests($orderID, &$requests, $mysqli) {
    $requests = array();
    $query = "SELECT ProductRequestID, Quantity, Notes, P.ProductID, Name, Price
        FROM ProductRequests R, Products P
        WHERE R.ProductID = P.ProductID
        AND OrderID = ?";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("i", $orderID);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $requests[] = $row;
    }
    $stmt->close();
    return true;
}

function confirmOrder($userID, &$orderID, $location, $datetime, $mysqli) {
    $query = "UPDATE Orders
        SET State = 'Sent', Location = ?, DeliveryDateTime = ?, RequestDateTime = NOW()
        WHERE CustomerID = ?
        AND State = 'Cart'";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("ssi", $location, $datetime, $userID);
    if (!$stmt->execute()) {
        return false;
    }
    $orderID = $mysqli->insert_id;
    $stmt->close();
    return true;
}

function removeFromCart($reqID, $mysqli) {
    $query = "DELETE FROM ProductRequests
    WHERE ProductRequestID = ?";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("i", $reqID);
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->close();
    return true;
}

function updateSupplierProfile($name, $email, $shortDesc, $longDesc, $address, $phone, $supplierID, $mysqli) {
    $stmt = $mysqli->prepare(
        "UPDATE Suppliers
        SET ShopName = ?, ShortDescription = ?, LongDescription = ?, Address = ?, PhoneNumber = ?
        WHERE UserID = ?");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("sssssi", $name, $shortDesc, $longDesc, $address, $phone, $supplierID);
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->close();
    if(!updateSupplierEmail($supplierID, $email, $mysqli)){
        return false;
    }
    return true;
}

function updateSupplierEmail($supplierID, $email, $mysqli){
    $stmt = $mysqli->prepare(
        "UPDATE Useraccounts
        SET Email = ?
        WHERE UserID = ?");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("si",$email, $supplierID);
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->close();
    return true;
}

function getRequestData($reqID, &$request, $mysqli) {
    $query = "SELECT CustomerID, SupplierID, Quantity, Notes, ProductID
        FROM ProductRequests R, Orders O
        WHERE R.OrderID = O.OrderID
        AND ProductRequestID = ?";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("i", $reqID);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    $request = $result->fetch_assoc();
    $stmt->close();
    return true;
}

function updateCartItemQuantity($reqID, $newQuantity, $mysqli) {
    $stmt = $mysqli->prepare(
        "UPDATE ProductRequests
        SET Quantity = ?
        WHERE ProductRequestID = ?");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("ii", $newQuantity, $reqID);
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->close();
    return true;
}

function getRegisteredSuppliers(&$suppliers, $mysqli){
    $suppliers = array();
    $query = "SELECT *
        FROM Suppliers";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $suppliers[] = $row;
    }
    $stmt->close();
    return true;
}

function updateSupplierState($supplierID, $newState, $mysqli){
    $stmt = $mysqli->prepare(
        "UPDATE Suppliers
        SET Approved = ?
        WHERE UserID = ?");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("ii", $newState, $supplierID);
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->close();
    return true;
}

function getSuppliersList(&$suppliers, $mysqli) {
    $suppliers = array();
    $query = "SELECT ShopName, ShortDescription, UserID
        FROM ApprovedSuppliers";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $suppliers[] = $row;
    }
    $stmt->close();
    return true;
}

function changeOrderState($oID, $oState, $mysqli){
    $stmt = $mysqli->prepare(
        "UPDATE Orders
        SET State = ?
        WHERE OrderID = ?");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("si", $oState, $oID );
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->close();
    return true;
}

function updateCustomerProfile($name, $email, $surname, $phone, $customerID, $mysqli) {
    $stmt = $mysqli->prepare(
        "UPDATE Customers
        SET Name = ?, Surname = ?, PhoneNumber = ?
        WHERE UserID = ?");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("sssi", $name, $surname, $phone, $customerID);
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->close();
    if(!updateCustomerEmail($customerID, $email, $mysqli)){
        return false;
    }
    return true;
}

function updateCustomerEmail($customerID, $email, $mysqli){
    $stmt = $mysqli->prepare(
        "UPDATE Useraccounts
        SET Email = ?
        WHERE UserID = ?");
    if(!$stmt) {
        return false;
    }
    $stmt->bind_param("si",$email, $customerID);
    if (!$stmt->execute()) {
        return false;
    }
    $stmt->close();
    return true;
}

function getSuppliersSearchResults($text, &$searchResults, $mysqli) {
    $searchResults = array();
    $pattern = "%$text%";
    $query = "SELECT DISTINCT S.ShopName, S.ShortDescription, S.UserID
        FROM RankedSuppliers S, AvailableProducts P, ProductCategories C
        WHERE S.UserID = P.SupplierID
        AND P.CategoryID = C.CategoryID
        AND (LOWER(S.ShopName) LIKE LOWER(?)
        OR LOWER(S.ShortDescription) LIKE LOWER(?)
        OR LOWER(C.CategoryName) LIKE LOWER(?))
        ORDER BY levenshtein(LOWER(S.ShopName), LOWER(?)), S.Ranking";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("ssss", $pattern, $pattern, $pattern, $text);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $searchResults[] = $row;
    }
    $stmt->close();
    return true;
}

function getProductsSearchResults($text, &$searchResults, $mysqli) {
    $searchResults = array();
    $pattern = "%$text%";
    $query = "SELECT P.ProductID, P.Name, P.Price, P.Description, S.UserID AS SupplierID, S.ShopName
        FROM RankedProducts P, ApprovedSuppliers S, ProductCategories C
        WHERE P.SupplierID = S.UserID
        AND P.CategoryID = C.CategoryID
        AND (LOWER(P.Name) LIKE LOWER(?)
        OR LOWER(P.Description) LIKE LOWER(?)
        OR LOWER(C.CategoryName) LIKE LOWER(?)
        OR LOWER(S.ShopName) LIKE LOWER(?))
        ORDER BY levenshtein(LOwER(P.Name), LOWER(?)), P.Ranking";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        echo "errore query";
        return false;
    }
    $stmt->bind_param("sssss", $pattern, $pattern, $pattern, $pattern, $text);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $searchResults[] = $row;
    }
    $stmt->close();
    return true;
}

function getSearchResults($text, &$searchResults, $mysqli) {
    $searchResults = array();
    if (!getSuppliersSearchResults($text, $suppliers, $mysqli)) {
        return false;
    }
    if (!getProductsSearchResults($text, $products, $mysqli)) {
        return false;
    }
    $searchResults['suppliers'] = $suppliers;
    $searchResults['products'] = $products;
    return true;
}

function getTopRankedSuppliers(&$suppliers, $count, $mysqli){
    $suppliers = array();
    $query = "SELECT UserID, ShopName, Ranking
        FROM RankedSuppliers
        ORDER BY Ranking DESC
        LIMIT ?";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("i",$count);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $suppliers[] = $row;
    }
    $stmt->close();
    return true;
}

function getTopRankedProducts(&$products, $count, $mysqli){
    $products = array();
    $query = "SELECT ProductID, Name, Ranking, Price, ShopName
        FROM RankedProducts P, ApprovedSuppliers S
        WHERE P.SupplierID = S.UserID
        ORDER BY Ranking DESC
        LIMIT ?";
    $stmt = $mysqli->prepare($query);
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param("i",$count);
    if (!$stmt->execute()) {
        return false;
    }
    $result = $stmt->get_result();
    while ($row = $result->fetch_assoc()) {
        $products[] = $row;
    }
    $stmt->close();
    return true;
}
?>
