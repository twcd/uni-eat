<?php
include "dbUtils.php";
include "secureSession.php";
include "misc.php";
include "errorChecker.php";
sec_session_start();
login_check($mysqli);

if (!isset($_GET['id'])) {
    if (userLoggedIn() && isSupplier()) {
        $userID = getUserID();
    } else {
        header("Location: home.php");
        exit;
    }
} else {
    $userID = $_GET['id'];
}
$result = getSupplierData($userID, $username, $email, $name, $shortDesc, $longDesc, $address, $phone, $mysqli);
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <title><?php echo $name ?> - UniEat</title>
        <?php include "mainInclusions.php" ?>
        <?php include "fileSystemUtils.php" ?>
        <link rel="stylesheet" href="stylesheets/mainSlideshow.css"/>
        <script type="text/javascript" src="scripts/toastNotification.js"></script>
        <script type="text/javascript" src="scripts/supplierProductsRemoval.js"></script>
        <script type="text/javascript" src="scripts/showProduct.js"></script>
        <script type="text/javascript" src="scripts/assignProduct.js"></script>
        <script type="text/javascript" src="scripts/showPath.js"></script>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div id="shopSlideshow" class="carousel slide main-slideshow" data-interval="8000" data-ride="carousel">
                <div class="carousel-inner h-100">
                    <div class="carousel-caption">
                        <h1><?php
                            echo $name;
                            if(isSupplier() and $userID == getUserID()) { ?>
                                <a href="supplierProfileUpdate.php">
                                    <span class="fas fa-edit fa-fw"></span>
                                    <span class="sr-only">Modifica profilo</span>
                                </a>
                            <?php } ?>
                        </h1>
                        <?php echo $shortDesc ?>
                    </div>
                    <?php $images = getSupplierImages($userID, ''); ?>
                    <div class="carousel-item active h-100">
                        <img class="center-cropped" src="<?php echo $images[0] ?>" alt="Supplier image"/>
                    </div>
                    <?php for ($i = 1; $i < count($images); $i++) { ?>
                        <div class="carousel-item h-100">
                            <img class="center-cropped" src="<?php echo $images[$i] ?>" alt="Supplier image"/>
                        </div>
                    <?php } ?>
                </div>
                <a class="carousel-control-prev" href="#shopSlideshow" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#shopSlideshow" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <div class="content">
                <?php if ($errNo == 1) { ?>
                    <div class="alert alert-danger" role="alert">
                        La registrazione è avvenuta con successo ma l'immagine non è stata salvata.
                        Riprova modificando il tuo profilo.
                    </div>
                <?php } ?>
                <section class="text-center">
                    <h2 class="border-bottom">Informazioni</h2>
                    <p>
                        <?php echo $longDesc ?>
                    </p>
                </section>
                <section class="text-center">
                    <h2 class="border-bottom">Contatti</h2>
                    <div class="container-fluid mb-3">
                        <div class="row">
                            <div class="col-sm-6 text-sm-right">
                                <strong>Email</strong>
                            </div>
                            <div class="col-sm-6 text-sm-left">
                                <?php echo $email ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 text-sm-right">
                                <strong>Indirizzo</strong>
                            </div>
                            <div class="col-sm-6 text-sm-left">
                                <?php echo $address ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 text-sm-right">
                                <strong>Telefono</strong>
                            </div>
                            <div class="col-sm-6 text-sm-left">
                                <?php echo $phone ?>
                            </div>
                        </div>
                    </div>
                </section>
                <section>
                    <h2 class="text-center border-bottom mb-0">Prodotti</h2>
                    <?php if(isSupplier() && getUserID() == $userID){ ?>
                        <div class="border-bottom pt-3">
                            <div class="mb-3 offset-sm-1 col-sm-10 offset-md-2 col-md-8">
                                <a href="supplierAddProduct.php" class="btn btn-primary btn-block btn-lg">Aggiungi prodotto</a>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if (getSupplierProducts($userID, $products, $mysqli) and count($products) > 0) { ?>
                        <div class="border-bottom py-2">
                            <?php foreach($products as $product) { ?>
                                <div class="lineRow d-flex p-2 p-md-3 flex-row">
                                    <span hidden class="productID"><?php echo $product['ProductID'];?></span>
                                    <div class="col-2 p-0">
                                        <div class="d-flex flex-column justify-content-center h-100">
                                            <div class="thumbnail">
                                                <a href class="imgThumbnail w-100 h-100">
                                                    <img class="center-cropped rounded-thumbnail" src="<?php echo getProductImagePath($product['ProductID'], ''); ?>" alt="Immagine di <?php echo $product['Name']?>">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="<?php echo ((isSupplier() && getUserID() == $userID) or isCustomer() or !userLoggedIn()) ? "col-8" : "col-10" ?> px-2">
                                        <div class="d-flex flex-row align-items-center justify-content-between">
                                            <div class="text-truncate">
                                                <strong><?php echo $product['Name']?></strong>
                                            </div>
                                            <span class="flex-shrink-0">
                                                <?php echo money($product['Price'])?>
                                            </span>
                                        </div>
                                        <div class="w-100 multi-line">
                                            <?php echo $product['Description'] ?>
                                        </div>
                                    </div>
                                    <?php if ((isSupplier() && getUserID() == $userID) or isCustomer() or !userLoggedIn()) { ?>
                                        <div class="col-2 p-0">
                                            <div class="d-flex flex-column justify-content-start h-100">
                                                <?php if (isSupplier() && getUserID() == $userID){?>
                                                    <a class="btn btn-outline-secondary mb-2 btn-sm" href="<?php echo "supplierProductModifier.php?productID=".$product['ProductID']; ?>">
                                                        <span class="fas fa-edit fa-fw"></span>
                                                        <span class="d-none d-md-block">Modifica</span>
                                                    </a>
                                                    <button class="btnRemove btn btn-outline-secondary btn-sm">
                                                        <span class="fas fa-trash-alt fa-fw"></span>
                                                        <span class="d-none d-md-block">Rimuovi</span>
                                                    </button>
                                                <?php } else if (isCustomer() or !userLoggedIn()){ ?>
                                                    <a class="btn btn-outline-secondary btn-sm" href="<?php echo "addToCart.php?id=".$product['ProductID']; ?>">
                                                        <span class="fas fa-cart-plus fa-fw"></span>
                                                        <span class="d-none d-md-block">Aggiungi al carrello</span>
                                                    </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } else { ?>
                        <div class="mt-2 alert alert-warning" role="alert">
                            Non è stato possibile reperire alcun prodotto.
                        </div>
                    <?php } ?>
                </section>
            </div>
        </div>
        <?php include "productPage.php"?>
        <?php include "footer.php" ?>

    </body>
</html>
