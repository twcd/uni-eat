<?php
include "dbUtils.php";
include "secureSession.php";
sec_session_start();
login_check($mysqli);
include "errorChecker.php";
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <script type="text/javascript" src="scripts/sha512.js"></script>
        <script type="text/javascript" src="scripts/forms.js"></script>
        <script type="text/javascript" src="scripts/register.js"></script>
        <title>Registrazione cliente - UniEat</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <section>
                    <h1>Registrazione cliente</h1>
                    <p>
                        Tramite quest'area potrai iscriverti al portale UniEat per usufruire dei nostri servizi.
                        Una volta registrato potrai effettuare ordini dai nostri fornitori e riceverli direttamente al campus.
                        Inoltre riceverai notifiche riguardanti i tuoi ordini in tempo reale.
                    </p>
                    <?php if ($errNo == 1) { ?>
                        <div class="alert alert-danger" role="alert">
                            Mancano alcuni parametri.
                        </div>
                    <?php } else if ($errNo == 2) { ?>
                        <div class="alert alert-danger" role="alert">
                            Impossibile creare il profilo.
                        </div>
                    <?php } ?>
                    <p>
                        Non sei un cliente? <a href="supplierRegistration.php">Registrati come fornitore</a>.
                    </p>
                    <form action="processCustomerRegistration.php" method="post" id="register-form">
                        <div class="form-group">
                            <label for="nameInput">Nome</label>
                            <input id="nameInput" class="form-control" type="text" name="name" required/>
                        </div>
                        <div class="form-group">
                            <label for="surnameInput">Cognome</label>
                            <input id="surnameInput" class="form-control" type="text" name="surname" required/>
                        </div>
                        <div class="form-group">
                            <label for="telInput">Numero di telefono</label>
                            <input id="telInput" class="form-control" type="tel" name="phone" required/>
                        </div>
                        <div class="form-group">
                            <label for="emailInput">Email</label>
                            <input id="emailInput" class="form-control" type="email" name="email" required/>
                        </div>
                        <div class="form-group">
                            <label for="usernameInput">Username</label>
                            <input id="usernameInput" class="form-control" type="text" name="username" required/>
                        </div>
                        <div class="form-group">
                            <label for="psw1">Password</label>
                            <input class="form-control" type="password" id="psw1" required/>
                            <div id="psw1Feedback">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="psw2">Conferma password</label>
                            <input class="form-control" type="password" id="psw2" required/>
                            <div id="psw2Feedback">
                            </div>
                        </div>
                        <input class="btn btn-primary" type="submit" value="Registrati" id="register-submit"/>
                    </form>
                </section>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
