<?php
include "dbUtils.php";
include "secureSession.php";
include "fileSystemUtils.php";
include "misc.php";
sec_session_start();
login_check($mysqli);

if (!userLoggedIn() || !isCustomer()) {
    $errorMessage = "Devi esserti autenticato come cliente per accedere al carrello.";
}
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <script type="text/javascript" src="scripts/customerCart.js"></script>
        <title>Il mio carrello - UniEat</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <?php if (isset($errorMessage)) { ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $errorMessage ?>
                    </div>
                <?php } else { ?>
                    <section>
                        <h1>Il mio carrello</h1>
                        <?php if (getCart(getUserID(), $cart, $mysqli)) { ?>
                            <?php if (count($cart) > 0) {
                                $total = 0; ?>
                                <div class="mb-3">
                                    <div class="offset-sm-1 col-sm-10 offset-md-2 col-md-8">
                                        <a href="confirmOrder.php" class="confirm-order btn btn-primary btn-lg btn-block">Procedi con l'ordine</a>
                                    </div>
                                </div>
                                <div class="my-2 border-top pt-2">
                                    <?php foreach($cart as $cartItem) {
                                        $localTotal = $cartItem['Quantity'] * $cartItem['Price'];
                                        $total += $localTotal;
                                        ?>
                                        <div class="lineRow d-flex p-2 p-md-3 flex-row">
                                            <span hidden class="reqID"><?php echo $cartItem['ReqID'];?></span>
                                            <div class="col-2 p-0">
                                                <div class="d-flex flex-column justify-content-center h-100">
                                                    <div class="thumbnail">
                                                        <img class="center-cropped rounded-thumbnail" src=<?php echo getProductImagePath($cartItem['ProductID'], "") ?> alt="Immagine di <?php echo $cartItem['Name']?>"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-8 px-2">
                                                <div class="d-flex flex-row align-items-center justify-content-between">
                                                    <div class="productName text-truncate">
                                                        <strong><?php echo $cartItem['Name']?></strong>
                                                    </div>
                                                    <div class="d-flex flex-row">
                                                        <button class="btn-minus btn btn-outline-secondary btn-sm">
                                                            <span class="fas fa-minus fa-fw"></span>
                                                            <span class="sr-only">Rimuovi</span>
                                                        </button>
                                                        <span class="align-self-center mx-2 item-quantity"><?php echo $cartItem['Quantity'] ?></span>
                                                        <button class="btn-plus btn btn-outline-secondary btn-sm">
                                                            <span class="fas fa-plus fa-fw"></span>
                                                            <span class="sr-only">Aggiungi</span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="item-price">
                                                    <?php echo money($cartItem['Price']) ?>
                                                </div>
                                                <div class="w-100 text-truncate">
                                                    <?php echo strlen($cartItem['Notes']) == 0 ? "-" : $cartItem['Notes'] ?>
                                                </div>
                                            </div>
                                            <div class="col-2 p-0">
                                                <div class="d-flex flex-column justify-content-start h-100">
                                                    <button class="btnRemove btn btn-outline-secondary btn-sm">
                                                        <span class="fas fa-trash-alt fa-fw"></span>
                                                        <span class="d-none d-md-block">Rimuovi</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <p class="border-top border-bottom h4 py-3 text-center">Totale: <span id="cartTotal" class="total-display"><?php echo money($total) ?></span></p>
                                <div class="offset-sm-1 col-sm-10 offset-md-2 col-md-8">
                                    <a href="confirmOrder.php" class="confirm-order btn btn-primary btn-lg btn-block">Procedi con l'ordine</a>
                                </div>
                            <?php } else { ?>
                                <div class="alert alert-info" role="alert">
                                    <p>Sembra che il tuo carrello sia vuoto.</p>
                                    <hr/>
                                    <p class="mb-0"><a href="suppliersList.php">Visita un fornitore</a> per iniziare a fare acquisti.</p>
                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="alert alert-info" role="alert">
                                C'è stato un problema nel reperire i dati riguardanti il tuo carrello.
                            </div>
                        <?php } ?>
                    </section>
                <?php } ?>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
