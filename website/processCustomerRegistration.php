<?php
include "dbUtils.php";
include "secureSession.php";

sec_session_start();

$username = $_POST['username'];
$password = $_POST['p'];
$email = $_POST['email'];
$phoneNumber = $_POST['phone'];
$name = $_POST['name'];
$surname = $_POST['surname'];

if (!isset($username, $password, $email, $phoneNumber, $name, $surname)) {
    header('Location: customerRegistration.php?error=1');
    exit;
}
if (!registerCustomer($username, $password, $name, $surname, $email, $phoneNumber, $mysqli)) {
    header('Location: customerRegistration.php?error=2');
    exit;
}
if (!login($username, $password, $mysqli)) {
    header('Location: login.php?error=1');
    exit;
}

header('Location: home.php');
?>
