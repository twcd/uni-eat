<?php
include "dbUtils.php";
include "secureSession.php";

sec_session_start();
login_check($mysqli);

$location = $_POST['location'];
$date = $_POST['date'];
$time = $_POST['time'];

if (!isset($location, $date, $time)) {
    header("Location: confirmOrder.php?error=1");
    exit;
}

if (!confirmOrder(getUserID(), $orderID, $location, "$date $time", $mysqli)) {
    header("Location: confirmOrder.php?error=2");
    exit;
}

header("Location: orderSuccess.php");
?>
