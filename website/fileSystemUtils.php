<?php
function saveProductImage($productID, $file, $basePath) {
    $uploaddir = $basePath.'images/products/'.$productID.'/';
    if (file_exists($uploaddir)) {
        removeProductImage($productID, $basePath);
    } else {
        mkdir($uploaddir, 0777, true);
    }
    if($file['error'] == 4){
        return true;
    }
    $uploadfile = $uploaddir . basename($file['name']);
    if(move_uploaded_file($file['tmp_name'], $uploadfile)){
        return true;
    } else {
        echo $file['tmp_name'];
        return false;
    }
}

function saveSupplierImage($supplierID, $file, $basePath) {
    if($file['error'][0] == 4){
        return true;
    }
    $uploaddir = $basePath.'images/supplier/'.$supplierID.'/';
    if (!file_exists($uploaddir)) {
        mkdir($uploaddir, 0777, true);
    }
    $ok = true;
    $items = count($file['name']);
    for ($i=0; $i<$items; $i++){
        $uploadfile = $uploaddir . basename($file['name'][$i]);
        $ok &= move_uploaded_file($file['tmp_name'][$i], $uploadfile);
    }
    return $ok;
}

function removeProductImageAndFolder($productID, $basePath){
    removeProductImage($productID, $basePath);
    rmdir($basePath.'images/products/'.$productID);
}

function removeProductImage($productID, $basePath) {
    $files = glob($basePath.'images/products/'.$productID.'/*');
    foreach($files as $file){
        unlink($file);
    }
}

function getProductImagePath($productID, $basePath){
    $file = glob($basePath.'images/products/'.$productID.'/*');
    if (count($file) <= 0){
        return $basePath.'images/noImage.jpg';
    }
    return $file[0];
}

function getSupplierImages($supplierID, $basePath) {
    $files = glob($basePath.'images/supplier/'.$supplierID.'/*');
    if (count($files) <= 0){
        return array($basePath.'images/noImage.jpg');
    } else {
        return $files;
    }
}

?>
