<?php
include "dbUtils.php";
include "secureSession.php";
sec_session_start();
login_check($mysqli);
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <title>### TEMPLATE ###</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
