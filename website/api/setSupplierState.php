<?php
include "../dbUtils.php";
include "../secureSession.php";
sec_session_start();
login_check($mysqli);

if(isset($_POST["id"], $_POST["state"])){
    $pID=$_POST["id"];
    $state=$_POST["state"];
    if(updateSupplierState($pID, $state, $mysqli)){
        echo 0;
    } else {
        echo 1;
    }
} else {
    echo -1;
}
?>
