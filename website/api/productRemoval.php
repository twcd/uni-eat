<?php
include "../dbUtils.php";
include "../secureSession.php";
include "../fileSystemUtils.php";
sec_session_start();
login_check($mysqli);

if(isset($_POST["id"])){
    $pID=$_POST["id"];
    if(removeProduct($pID, $mysqli)){
        removeProductImageAndFolder($pID, '../');
        echo 0;
    } else {
        echo 1;
    }
} else {
    echo -1;
}
?>
