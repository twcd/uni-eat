<?php
include "../dbUtils.php";
include "../secureSession.php";

function doChangeQuantity($reqID, $quantity, $mysqli) {
    if (!isset($reqID, $quantity)) {
        return 1;
    }
    if (!userLoggedIn() or !isCustomer()) {
        return 2;
    }
    if (!getRequestData($reqID, $request, $mysqli) or $request['CustomerID'] != getUserID()) {
        return 3;
    }
    if (!updateCartItemQuantity($reqID, $quantity, $mysqli)) {
        return 4;
    }
    return 0;
}

sec_session_start();
login_check($mysqli);

header("Content-Type: application/json");

$result = array();
$result['error'] = doChangeQuantity($_GET['id'], $_GET['quantity'], $mysqli);
print json_encode($result);
?>
