<?php
include "../dbUtils.php";
include "../secureSession.php";

function doAddToCart($productID, $quantity, $notes, $mysqli) {
    if (!isset($productID, $quantity)) {
        return 1;
    }
    if (!isset($notes)) {
        $notes = "";
    }
    if (!userLoggedIn() or !isCustomer()) {
        return 2;
    }
    if (!addProductToCart(getUserID(), $productID, $quantity, $notes, $mysqli)) {
        return 3;
    }
    return 0;
}

sec_session_start();
login_check($mysqli);

header("Content-Type: application/json");

$result = array();
$result['error'] = doAddToCart($_GET['id'], $_GET['quantity'], $_GET['notes'], $mysqli);
print json_encode($result);
?>
