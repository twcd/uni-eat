<?php
include "../dbUtils.php";
include "../secureSession.php";
include "../fileSystemUtils.php";
sec_session_start();

if(isset($_GET["id"])){
    $pID=$_GET["id"];
    if(getSingleProduct($pID, $product, $mysqli)){
        $error = 0;
    } else {
        $error = 1;
    }
} else {
    $error = 2;
}

header("Content-Type: application/json");

$product['imageSrc'] = substr(getProductImagePath($pID, '../'), 3);
$result['error'] = $error;
$result['product'] = $product;
print json_encode($result);
?>
