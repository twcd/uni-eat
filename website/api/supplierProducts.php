<?php
header('Content-Type: application/json');
include "../dbUtils.php"
if(isset($_GET["id"])){
    $uID=$_GET["id"];
    $stmt = $conn->prepare("SELECT ProductID, Name, Description, Price
        FROM Products P, ProductCategories C
        WHERE SupplierID = ?
        AND P.CategoryID = C.CategoryID");

    $stmt->bind_param("i",$uID);
    $stmt->execute();
    $result = $stmt->get_result();
    $output = array();

    while($row = $result->fetch_assoc()) {
        $row['imageSrc'] = glob('images/products/'.$row['ProductID'].'/*');
        $output[] = $row;
    }
    $stmt->close();
    print json_encode($output);
}
?>
