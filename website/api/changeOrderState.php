<?php
include "../dbUtils.php";
include "../secureSession.php";
sec_session_start();
login_check($mysqli);

if(isset($_POST["id"], $_POST["state"])){
    $oID=$_POST["id"];
    $oState=$_POST["state"];
    if(changeOrderState($oID, $oState, $mysqli)){
        echo 0;
    } else {
        echo 1;
    }
} else {
    echo -1;
}
?>
