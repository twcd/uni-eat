<?php
include "../dbUtils.php";
include "../secureSession.php";
sec_session_start();
login_check($mysqli);

$notifications = array();
$notificationsCount = 0;

if (!userLoggedIn()) {
    $error = 1;
} else {
    if (isset($_GET['afterid'])) {
        $afterID = $_GET['afterid'];
    } else {
        $afterID = 0;
    }
    if (isset($_GET['page'])) {
        $pageNum = $_GET['page'];
    } else {
        $pageNum = 0;
    }
    if (isset($_GET['count'])) {
        $count = $_GET['count'];
    } else {
        $count = 10;
    }
    if (!getNotifications(getUserID(), $notifications, $pageNum, $count, $afterID, $mysqli) or
        !getNotificationsCount(getUserID(), $notificationsCount, $mysqli)) {
        $error = 2;
    } else {
        $error = 0;
    }
}

header("Content-Type: application/json");

$result['count'] = $notificationsCount;
$result['error'] = $error;
$result['notifications'] = $notifications;

print json_encode($result);
?>
