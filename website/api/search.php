<?php
include "../dbUtils.php";
include "../secureSession.php";
sec_session_start();

$query = $_GET['q'];

header("Content-Type: application/json");

if (!getSearchResults($query, $searchResults, $mysqli)) {
    $error = 1;
} else {
    $error = 0;
}

$result = array();
$result['results'] = $searchResults;
$result['error'] = $error;

print json_encode($result);
?>
