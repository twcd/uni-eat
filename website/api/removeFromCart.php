<?php
include "../dbUtils.php";
include "../secureSession.php";

function doRemove($reqID, $mysqli) {
    if (!isset($reqID)) {
        return 1;
    }
    if (!userLoggedIn() or getUserType() != "customer") {
        return 2;
    }
    if (!removeFromCart($reqID, $mysqli)) {
        return 3;
    }
    return 0;
}

sec_session_start();
login_check($mysqli);

header("Content-Type: application/json");

$result = array();
$result['error'] = doRemove($_GET['id'], $mysqli);
print json_encode($result);
?>
