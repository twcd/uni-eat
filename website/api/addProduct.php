<?php
include "../dbUtils.php";
include "../secureSession.php";
include "../fileSystemUtils.php";
sec_session_start();
login_check($mysqli);

function insertProduct($name, $description, $price, $catID, $mysqli){
    if(!isset($name, $price, $catID)){return 2;}
    if(!userLoggedIn()){ return 3;}
    if(!isset($description)){
        $description = "";
    }
    if(!registerProduct($name, $description, $price, getUserID(), $catID, $productID, $mysqli)){
        return 1;
    }
    if (!saveProductImage($productID, $_FILES['imageSrc'], '../')) {
        return 2;
    }
    return 0;
}

header("Content-Type: application/json");

$result = array();
$result['error'] = insertProduct($_POST["name"], $_POST["description"], $_POST["price"], $_POST["category"], $mysqli);
print json_encode($result);
?>
