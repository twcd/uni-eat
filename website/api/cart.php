<?php
include "../dbUtils.php";
include "../secureSession.php";
sec_session_start();
login_check($mysqli);

if (!userLoggedIn() || !isCustomer()) {
    $error = 1;
    $cart = array();
} else {
    if (!getCart(getUserID(), $cart, $mysqli)) {
        $error = 2;
    } else {
        $error = 0;
    }
}

header("Content-Type: application/json");

$result["error"] = $error;
$result["cart"] = $cart;

print json_encode($result);
?>
