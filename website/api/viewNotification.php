<?php
include "../dbUtils.php";
include "../secureSession.php";
sec_session_start();
login_check($mysqli);

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    if (!viewNotification($id, $mysqli)) {
        $error = 2;
    } else {
        $error = 0;
    }
} else {
    $error = 1;
}

header("Content-Type: application/json");

$result = array();
$result['error'] = $error;

print json_encode($result);
?>
