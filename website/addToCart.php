<?php
include "dbUtils.php";
include "secureSession.php";
include "misc.php";
include "errorChecker.php";
sec_session_start();
login_check($mysqli);

if (!isset($_GET['id'])) {
    $errorMessage = "Nessun ID prodotto specificato.";
} else if (!getSingleProduct($_GET['id'], $product, $mysqli)) {
    $errorMessage = "Il prodotto che hai selezionato sembra non essere presente.";
}
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <title>Aggiungi al carrello - UniEat</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <section>
                    <h1>Aggiungi al carrello</h1>
                    <?php if (isset($errorMessage)) { ?>
                        <div class="alert alert-danger" role="alert">
                            <?php echo $errorMessage ?>
                        </div>
                    <?php } else { ?>
                        <?php if ($errNo == 1) { ?>
                            <div class="alert alert-danger" role="alert">
                                Impossibile aggiungere al carrello.
                            </div>
                        <?php } ?>
                        <p>
                            Nome: <?php echo $product['Name'] ?>
                            <br/>
                            Prezzo: <?php echo money($product['Price']) ?>
                        </p>
                        <form action="processAddToCart.php" method="get">
                            <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>">
                            <div class="form-group">
                                <label for="quantityInput">Quantità</label>
                                <input class="form-control" id="quantityInput" type="number" name="quantity" value="1" min="1"/>
                            </div>
                            <div class="form-group">
                                <label for="notesInput">Note/Modifiche</label>
                                <textarea class="form-control" id="notesInput" name="notes" rows="5"></textarea>
                            </div>
                            <input class="btn btn-primary" type="submit" value="Aggiungi al carrello">
                        </form>
                    <?php } ?>
                </section>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
