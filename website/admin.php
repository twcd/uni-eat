<?php
include "dbUtils.php";
include "secureSession.php";
include "fileSystemUtils.php";
include "misc.php";
sec_session_start();
login_check($mysqli);
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <script type="text/javascript" src="scripts/manageSuppliers.js"></script>
        <title>UniEat - ADMIN</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <?php if (!userLoggedIn() || !isAdmin()) { ?>
                    <div class="alert alert-danger" role="alert">
                        Devi esserti autenticato come Admin per accedere a questa pagina.
                    </div>
                <?php } else { ?>
                    <section>
                        <h1>Pannello di controllo amministratore</h1>
                        <p>
                            In questa pagina puoi amministrare i fornitori registrati al portale UniEat.
                        </p>
                        <p>
                            Puoi decidere di attivare o disabilitare i loro account, rendendoli visibili o meno ai clienti.
                        </p>
                        <?php if(getRegisteredSuppliers($suppliers, $mysqli)){?>
                            <div class="border-bottom py-2">
                                <?php foreach($suppliers as $supplier){ ?>
                                    <div class="lineRow d-flex p-2 p-md-3 flex-row">
                                        <span hidden class="supplierID"><?php echo $supplier['UserID'];?></span>
                                        <div class="col-2 p-0">
                                            <div class="d-flex flex-column justify-content-center h-100">
                                                <a class="thumbnail" href="<?php echo "supplier.php?id=".$supplier['UserID'] ?>">
                                                    <img class="center-cropped rounded-thumbnail" src="<?php echo getSupplierImages($supplier['UserID'], "")[0] ?>" alt="Immagine del profilo di <?php echo $supplier['ShopName']?>"/>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="col-8 px-2">
                                            <div class="d-flex flex-row align-items-center justify-content-between">
                                                <div class="productName text-truncate">
                                                    <a class="h3" href="<?php echo "supplier.php?id=".$supplier['UserID'] ?>"><?php echo $supplier['ShopName']?></a>
                                                </div>
                                            </div>
                                            <div class="w-100 text-truncate">
                                                <?php echo $supplier['ShortDescription'] ?>
                                            </div>
                                        </div>
                                        <div class="col-2 buttons d-flex align-items-center justify-content-center px-0">
                                            <button class="btnApprove btn btn-outline-success w-100 <?php echo $supplier['Approved'] == 1 ? "d-none" : "" ?>">
                                                <span class="far fa-check-square fa-fw"></span>
                                                <span class="d-none d-md-block">Abilita</span>
                                            </button>
                                            <button class="btnRemove btn btn-outline-danger w-100 <?php echo $supplier['Approved'] == 0 ? "d-none" : "" ?>">
                                                <span class="fas fa-trash-alt fa-fw"></span>
                                                <span class="d-none d-md-block">Disabilita</span>
                                            </button>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } else { ?>
                            <div class="mt-2 alert alert-warning" role="alert">
                                Non è stato possibile reperire alcun fornitore.
                            </div>
                        <?php } ?>
                    <?php } ?>
                </section>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
