<nav class="navbar navbar-dark bg-primary navbar-expand-md fixed-top shadow-sm">
    <div class="container-fluid px-0 d-flex">
        <!-- Brand -->
        <div class="navbar-header">
            <a class="navbar-brand mx-0 my-0" href="home.php">
                <img src="images/unieat-1.png" id="unieatLogo" alt="UniEat Logo">
            </a>
        </div>


        <?php if (!userLoggedIn()) { ?>
            <div class="ml-auto">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="login.php">
                            <span class="fas fa-sign-in-alt fa-fw"></span> Login
                        </a>
                    </li>
                </ul>
            </div>
        <?php } else { ?>
            <div class="ml-auto d-md-none">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="logout.php">
                            <span class="fas fa-sign-out-alt fa-fw"></span> Logout
                        </a>
                    </li>
                </ul>
            </div>
        <?php } ?>

        <!-- Dropdown navbar -->
        <button class="navbar-toggler ml-2" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
            <span class="badge badge-pill badge-danger notification-count notification-badge">3</span>
            <span class="sr-only">notifiche</span>
        </button>

        <!-- Links -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
            <?php if (userLoggedIn()) { ?>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                        <span class="fas fa-user fa-fw"></span>
                        <?php echo getUserCompleteName(getUserID(), $mysqli) ?>
                    </a>
                    <div class="dropdown-menu navbar-dropdown">
                        <?php if (isCustomer()) { ?>
                            <a class="dropdown-item" href="customer.php">
                                <span class="fas fa-user fa-fw"></span>
                                Il mio profilo
                            </a>
                            <a class="dropdown-item" href="cart.php">
                                <span class="fas fa-shopping-cart fa-fw"></span>
                                Il mio carrello
                            </a>
                            <a class="dropdown-item" href="ordersListPage.php">
                                <span class="fas fa-clipboard-list fa-fw"></span>
                                I miei ordini
                            </a>
                        <?php } else if (isSupplier()) { ?>
                            <a class="dropdown-item" href="supplier.php">
                                <span class="fas fa-image fa-fw"></span>
                                Pagina del locale
                            </a>
                            <a class="dropdown-item" href="supplierProfileUpdate.php">
                                <span class="fas fa-edit fa-fw"></span>
                                Gestione profilo
                            </a>
                            <a class="dropdown-item" href="ordersListPage.php">
                                <span class="fas fa-clipboard-list fa-fw"></span>
                                Elenco ordini
                            </a>
                        <?php } else if (isAdmin()) { ?>
                            <a class="dropdown-item" href="admin.php">
                                <span class="fas fa-user fa-fw"></span>
                                Pannello di controllo
                            </a>
                        <?php } ?>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="logout.php">
                            <span class="fas fa-sign-out-alt fa-fw"></span>
                            Logout
                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
                        <span class="fas fa-bell fa-fw"></span>
                        Notifiche
                        <span class="badge badge-pill badge-danger notification-count">
                            <?php
                            if (getNotificationsCount(getUserID(), $count, $mysqli) and $count > 0) {
                                echo $count;
                            }
                            ?>
                        </span>
                    </a>
                    <div class="dropdown-menu">
                        <div class="notifications-present">
                            <div class="notifications-container"></div>
                            <a class="dropdown-item border-top" href="notifications.php">Mostra tutte</a>
                        </div>
                        <div class="dropdown-item disabled notifications-absent">
                            Non hai ancora ricevuto notifiche
                        </div>
                    </div>
                </li>
            <?php } ?>
                <li class="nav-item">
                    <a class="nav-link" href="suppliersList.php">
                        <span class="fas fa-book-open fa-fw"></span>
                        Fornitori
                    </a>
                </li>
            </ul>
        </div>

        <!-- Searchbar -->
        <div class="d-none d-md-inline">
            <form action="search.php" class="search-form" method="get">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Cerca in UniEat" aria-label="Cerca in UniEat" required/>
                    <div class="input-group-append">
                        <button class="btn btn-secondary" type="submit"><span class="fas fa-search"></span> Cerca</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</nav>

<div class="d-md-none bg-primary p-2 shadow-sm">
    <form action="search.php" method="get" class="search-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Cerca in UniEat" aria-label="Cerca in UniEat" required/>
            <div class="input-group-append">
                <button class="btn btn-secondary" type="submit"><i class="fas fa-search"></i><span class="sr-only">Cerca</span></button>
            </div>
        </div>
    </form>
</div>
