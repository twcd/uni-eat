<div class="modal fade" id="productPopUp" tabindex="-1" role="dialog" aria-labelledby="popUp" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <label class="productTitle modal-title"></label>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-6 col-md-4">
                        <div class="thumbnail">
                            <img class="productImg center-cropped rounded-thumbnail" src="images/noImage.jpg" alt="Immagine mancante">
                        </div>
                    </div>
                    <div class="col-6 col-md-8">
                        <strong class="productName"></strong>
                        <div class="productPrice"></div>
                    </div>
                </div>
                <div class="productDesctiption mt-3"></div>
            </div>
            <div class="modal-footer">
                <?php if(!userLoggedIn() or isCustomer()){ ?>
                    <a id="toCart" class="btn btn-secondary btn-sm" href="">Aggiungi al carrello</a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
