<?php
function money($value) {
    return "€ ".number_format($value, 2, ',', ' ');
}

function orderStateString($state) {
    switch ($state) {
        case "Cart":
            return "In carrello";
        case "Sent":
            return "In attesa di approvazione";
        case "Accepted":
            return "Accettato";
        case "Delivering":
            return "In consegna";
        case "Delivered":
            return "Consegnato";
        case "Declined":
                return "Rifiutato";
        case "Invalid":
            return "Non valido";
        default:
            return "SCONOSCIUTO";
    }
}

function formatDateTime($dateTime) {
    return (new DateTime($dateTime))->format('d/m/y - H:i');
}
?>
