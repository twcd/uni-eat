<?php
include "dbUtils.php";
include "secureSession.php";
include "fileSystemUtils.php";
sec_session_start();
login_check($mysqli);

if (!getSuppliersList($suppliers, $mysqli)) {
    $errorMessage = "Non è stato possibile recuperare l'elenco dei fornitori.";
}

?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <title>UniEat - Fornitori</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <section>
                    <h1>I nostri fornitori</h1>
                    <?php if (isset($errorMessage)) { ?>
                        <div class="alert alert-danger" role="alert">
                            <?php echo $errorMessage ?>
                        </div>
                    <?php } else { ?>
                        <div class="py-2 border-top border-bottom">
                            <?php foreach ($suppliers as $supplier) { ?>
                                <div class="lineRow d-flex p-2 p-md-3 flex-row">
                                    <div class="col-2 p-0">
                                        <div class="d-flex flex-column justify-content-center h-100">
                                            <a class="thumbnail" href="<?php echo "supplier.php?id=".$supplier['UserID'] ?>">
                                                <img class="center-cropped rounded-thumbnail" src="<?php echo getSupplierImages($supplier['UserID'], "")[0] ?>" alt="Immagine del profilo di <?php echo $supplier['ShopName']?>"/>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-10 px-2">
                                        <div class="d-flex flex-row align-items-center justify-content-between">
                                            <div class="productName text-truncate">
                                                <a class="h3" href="<?php echo "supplier.php?id=".$supplier['UserID'] ?>"><?php echo $supplier['ShopName']?></a>
                                            </div>
                                        </div>
                                        <div class="w-100 text-truncate">
                                            <?php echo $supplier['ShortDescription'] ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>

                    <?php } ?>
                </section>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
