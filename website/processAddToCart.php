<?php
include "dbUtils.php";
include "secureSession.php";

sec_session_start();
login_check($mysqli);

$productID = $_GET['id'];
$quantity = $_GET['quantity'];
$notes = $_GET['notes'];

if (!isset($productID, $quantity)) {
    header("Location: addToCart.php?error=1");
    exit;
}
if (!isset($notes)) {
    $notes = "";
}
if (!userLoggedIn() or getUserType() != "customer") {
    setLoginCallbackPage("processAddToCart.php?id=$productID&quantity=$quantity&notes=$notes", "home.php", "customer");
    header("Location: login.php");
    exit;
}
if (!addProductToCart(getUserID(), $productID, $quantity, $notes, $mysqli)) {
    header("Location: addToCart.php?id=$productID&error=1");
    exit;
}

header("Location: cart.php?new");

?>
