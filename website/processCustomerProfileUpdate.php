<?php
include "dbUtils.php";
include "secureSession.php";

sec_session_start();
login_check($mysqli);

if (!userLoggedIn()) {
    header('Location: login.php');
    exit;
}
$name = $_POST['name'];
$surname = $_POST['surname'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$customerID = $_POST['customerID'];

if (!isCustomer()) {
    header('Location: customerProfileUpdate.php?error=3');
    exit;
}

if (!isset($name, $surname, $email, $phone, $customerID)) {
    header('Location: customerProfileUpdate.php?error=1');
    exit;
}

if (!updateCustomerProfile($name, $surname, $email, $phone, $customerID, $mysqli)) {
    header('Location: customerProfileUpdate.php?error=2');
    exit;
}
header("Location: customer.php");
?>
