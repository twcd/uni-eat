<?php
function sec_session_start() {
    $session_name = 'sec_session_id';
    $secure = false;
    $httponly = true;
    ini_set('session.use_only_cookies', 1);
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
    session_name($session_name);
    session_start();
    session_regenerate_id();
}

function login($username, $password, $mysqli) {
    $stmt = $mysqli->prepare("SELECT UserID, Email, PasswordHash, PasswordSalt, UserType FROM UserAccounts WHERE Username = ? LIMIT 1");
    if (!$stmt) {
        return false;
    }
    $stmt->bind_param('s', $username);
    $stmt->execute();
    $stmt->store_result();
    $stmt->bind_result($user_id, $email, $db_password, $salt, $type);
    $stmt->fetch();
    $password = hash('sha512', $password.$salt);
    if($stmt->num_rows != 1 or $db_password != $password) {
        return false;
    }
    $user_browser = $_SERVER['HTTP_USER_AGENT'];
    $user_id = preg_replace("/[^0-9]+/", "", $user_id);
    $_SESSION['user_id'] = $user_id;
    $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username);
    $_SESSION['username'] = $username;
    $_SESSION['user_type'] = $type;
    $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
    return true;
}

function login_check($mysqli) {
    if(!isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string'], $_SESSION['user_type'])) {
        unsetUser();
        return;
    }
    $user_id = $_SESSION['user_id'];
    $login_string = $_SESSION['login_string'];
    $username = $_SESSION['username'];
    $user_browser = $_SERVER['HTTP_USER_AGENT'];
    $stmt = $mysqli->prepare("SELECT PasswordHash FROM UserAccounts WHERE UserID = ? LIMIT 1");
    if (!$stmt) {
        unsetUser();
        return;
    }
    $stmt->bind_param('i', $user_id);
    $stmt->execute();
    $stmt->store_result();
    if($stmt->num_rows != 1) {
        unsetUser();
        return;
    }
    $stmt->bind_result($password);
    $stmt->fetch();
    $login_check = hash('sha512', $password.$user_browser);
    if ($login_check != $login_string) {
        unsetUser();
    }
}

function unsetUser() {
    unset($_SESSION['user_id']);
    unset($_SESSION['username']);
    unset($_SESSION['login_string']);
    unset($_SESSION['user_type']);
}

function preprocessPassword($rawPassword, &$salt) {
    $salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
    return hash('sha512', $rawPassword.$salt);
}

function fakeLogin() {
    $_SESSION['user_id'] = 0;
    $_SESSION['username'] = "fake";
    $_SESSION['login_string'] = "fake_login";
}

function userLoggedIn() {
    return isset($_SESSION['user_id'], $_SESSION['username'], $_SESSION['login_string']);
}

function getUsername() {
    if (!userLoggedIn()) {
        return "";
    }
    return $_SESSION['username'];
}

function getUserID() {
    if (!userLoggedIn()) {
        return 0;
    }
    return $_SESSION['user_id'];
}

function getUserType() {
    if (!userLoggedIn()) {
        return "guest";
    }
    return $_SESSION['user_type'];
}

function isCustomer() {
    return getUserType() == "customer";
}

function isSupplier() {
    return getUserType() == "supplier";
}

function isAdmin() {
    return getUserType() == "admin";
}

function setLoginCallbackPage($url, $alt) {
    $_SESSION['login_callback'] = $url;
    $_SESSION['login_alt'] = $alt;
    $numRoles = func_num_args();
    $roles = array();
    for ($i = 2; $i < $numRoles; $i++) {
        $roles[] = func_get_arg($i);
    }
    $_SESSION['login_required_roles'] = $roles;
}

function getLoginCallbackPage() {
    $type = getUserType();
    if (!isset($_SESSION['login_required_roles'], $_SESSION['login_callback'], $_SESSION['login_alt'])) {
        if (isSupplier()) {
            return "supplier.php";
        } else {
            return "home.php";
        }
    }
    if (count($_SESSION['login_required_roles']) == 0) {
        return $_SESSION['login_callback'];
    }
    foreach ($_SESSION['login_required_roles'] as $role) {
        if ($role == $type) {
            return $_SESSION['login_callback'];
        }
    }
    return $_SESSION['login_alt'];
}
?>
