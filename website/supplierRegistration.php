<?php
include "dbUtils.php";
include "secureSession.php";
sec_session_start();
login_check($mysqli);
include "errorChecker.php";
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <script type="text/javascript" src="scripts/sha512.js"></script>
        <script type="text/javascript" src="scripts/forms.js"></script>
        <script type="text/javascript" src="scripts/register.js"></script>
        <script type="text/javascript" src="scripts/showPath.js"></script>
        <title>Registrazione fornitore - UniEat</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <section>
                    <h1>Registrazione fornitore</h1>
                    <?php if ($errNo == 1) { ?>
                        <div class="alert alert-danger" role="alert">
                            Mancano alcuni parametri.
                        </div>
                    <?php } else if ($errNo == 2) { ?>
                        <div class="alert alert-danger" role="alert">
                            Impossibile effettuare la registrazione. Riprova.
                        </div>
                    <?php } ?>
                    <p>
                        Tramite quest'area puoi entrare a far parte di UniEat come fornitore.
                        Iscrivendoti alla piattaforma permetterai agli studenti del Campus di Cesena di effettuare ordini presso la tua attività.
                    </p>
                    <p>
                        Ricorda che appena terminata la procedura il tuo account non sarà abilitato fino a quando uno dei nostri amministratori non accetterà la richiesta.
                    </p>
                    <p>
                        Non sei un fornitore? <a href="customerRegistration.php">Registrati come cliente</a>.
                    </p>
                    <form action="processSupplierRegistration.php" method="post" id="register-form" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="nameInput">Nome dell'attività</label>
                            <input id="nameInput" class="form-control" type="text" name="name" required/>
                        </div>
                        <div class="form-group">
                            <label for="shortDescInput">Descrizione breve (massimo 100 caratteri)</label>
                            <textarea id="shortDescInput" class="form-control" name="shortDesc" rows="2" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="longDescInput">Descrizione lunga (massimo 800 caratteri)</label>
                            <textarea id="longDescInput" class="form-control" name="longDesc" rows="5" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="phoneInput">Numero di telefono</label>
                            <input id="phoneInput" class="form-control" type="tel" name="phone" required/>
                        </div>
                        <div class="form-group">
                            <label for="addressInput">Indirizzo</label>
                            <input id="addressInput" class="form-control" type="text" name="address" required/>
                        </div>
                        <div class="form-group">
                            <label for="emailInput">Email</label>
                            <input id="emailInput" class="form-control" type="email" name="email" required/>
                        </div>
                        <div class="form-group">
                            <label for="usernameInput">Username</label>
                            <input id="usernameInput" class="form-control" type="text" name="username" required/>
                        </div>
                        <div class="form-group">
                            <label for="psw1">Password</label>
                            <input type="password" id="psw1" class="form-control" required/>
                            <div id="psw1Feedback">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="psw2">Conferma password</label>
                            <input type="password" id="psw2" class="form-control" required/>
                            <div id="psw2Feedback">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="imageFile">Immagini del negozio</label>
                            <div class="custom-file">
                                <input id="imageFile" type="file" class="custom-file-input" name="imageSrc[]" multiple>
                                <label class="custom-file-label" for="imageFile">Scegli un file...</label>
                            </div>
                        </div>
                        <input class="btn btn-primary" type="submit" value="Registrati" id="register-submit"/>
                    </form>
                </section>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
