<?php
include "dbUtils.php";
include "secureSession.php";
sec_session_start();
login_check($mysqli);
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <title>Aggiungi prodotto - UniEat</title>
        <script type="text/javascript" src="scripts/showPath.js"></script>
        <script type="text/javascript" src="scripts/addProduct.js"></script>
    </head>
    <body>
        <?php include "navbar.php" ?>

        <div class="main-container">
            <div class="content">
                <?php if (!userLoggedIn() || !isSupplier()) { ?>
                    <div class="alert alert-danger" role="alert">
                        Devi esserti autenticato come fornitore per accedere a questa pagina.
                    </div>
                <?php } else { ?>
                    <section>
                        <h1>Aggiungi prodotto</h1>
                        <form id="data" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="productName">Nome prodotto</label>
                                <input id="productName" class="form-control" type="text" name="name" required/>
                            </div>
                            <div class="form-group">
                                <label for="productDesc">Descrizione prodotto</label>
                                <input id="productDesc" class="form-control" type="text" name="description" required/>
                            </div>
                            <div class="form-group">
                                <label for="productPrice">Prezzo</label>
                                <input id="productPrice" class="form-control" type="number" name="price" step="any" required>
                            </div>
                            <?php if (getCategories($categories, $mysqli)) { ?>
                                <div class="form-group">
                                    <label for="productCategory">Categoria</label>
                                    <select id="productCategory" class="form-control" name="category" required>
                                        <?php foreach($categories as $category) { ?>
                                            <option value="<?php echo $category['CategoryID']?>"><?php echo $category['CategoryName']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            <?php } else { ?>
                                <div class="alert alert-danger" role="alert">
                                    Non è stato possibile reperire le categorie.
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <div class="custom-file">
                                    <input id="imageFile" type="file" class="custom-file-input" name="imageSrc">
                                    <label class="custom-file-label" for="imageFile">Scegli un file...</label>
                                </div>
                            </div>
                            <input class="btn btn-primary" type="submit" value="Aggiungi"/>
                            <a class="btn btn-secondary" href="supplier.php">Torna indietro</a>
                        </form>
                    </section>
                <?php } ?>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
