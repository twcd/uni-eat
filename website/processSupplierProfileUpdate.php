<?php
include "dbUtils.php";
include "secureSession.php";
include "fileSystemUtils.php";

sec_session_start();
login_check($mysqli);

if (!userLoggedIn()) {
    header('Location: login.php');
    exit;
}

$name = $_POST['name'];
$email = $_POST['email'];
$shortDesc = $_POST['shortDesc'];
$longDesc = $_POST['longDesc'];
$address = $_POST['address'];
$phone = $_POST['phone'];
$supplierID = $_POST['supplierID'];

if (!isSupplier()) {
    header('Location: supplierProfileUpdate.php?error=3');
    exit;
}

if (!isset($name, $email, $shortDesc, $longDesc, $address, $phone, $supplierID)) {
    header('Location: supplierProfileUpdate.php?error=1');
    exit;
}

if (!updateSupplierProfile($name, $email, $shortDesc, $longDesc, $address, $phone, $supplierID, $mysqli)) {
    header('Location: supplierProfileUpdate.php?error=2');
    exit;

}
if (!saveSupplierImage($supplierID, $_FILES['imageSrc'], '')) {
    header('Location: supplierProfileUpdate.php?error=4');
    exit;
}
header("Location: supplier.php");
?>
