<?php
include "dbUtils.php";
include "secureSession.php";
sec_session_start();
login_check($mysqli);

if (userLoggedIn()) {
    $userID = getUserID();
} else {
    header("Location: home.php");
    exit;
}
if (!getCustomerData($userID, $username, $email, $name, $surname, $phone, $mysqli)) {
    $errorMessage = "Il cliente con ID $userID non è stato trovato. Forse hai modificato manualmente l'URL?";
}
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <title><?php echo $username ?> - UniEat - Gestione</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <?php if (isset($errorMessage)) { ?>
                    <div class="alert alert-danger" role="alert">
                        <?php echo $errorMessage ?>
                    </div>
                <?php } else {?>
                    <form action="processCustomerProfileUpdate.php" method="post">
                        <input type="hidden" name="customerID" value="<?php echo $userID?>" />
                        <div class="form-group">
                            <label for="nameInput">Nome</label>
                            <input id="nameInput" class="form-control" type="text" name="name" value="<?php echo $name ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="surnameInput">Cognome</label>
                            <input id="surnameInput" class="form-control" type="text" name="surname" value="<?php echo $surname ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="emailInput">Email</label>
                            <input id="emailInput" class="form-control" type="text" name="email" value="<?php echo $email ?>"/>
                        </div>
                        <div class="form-group">
                            <label for="phoneInput">Telefono</label>
                            <input id="phoneInput" class="form-control" type="text" name="phone" value="<?php echo $phone ?>"/>
                        </div>
                        <input class="btn btn-primary" type="submit" value="Aggiorna"/>
                        <a class="btn btn-secondary" href="<?php echo "customer.php"?>">Annulla</a>
                    </form>
                <?php } ?>

            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
