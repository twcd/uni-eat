<?php
include "dbUtils.php";
include "secureSession.php";
sec_session_start();
login_check($mysqli);

?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <title></title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <div class="alert alert-success" role="alert">
                    Ordine effettuato con successo! Grazie per il tuo acquisto.
                    <a href="ordersListPage.php" class="alert-link">Visualizza Ordini</a>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4 mb-2 mb-md-0">
                            <a class="btn btn-secondary btn-block" href="home.php">Torna alla home</a>
                        </div>
                        <div class="col-md-4 mb-2 mb-md-0">
                            <a class="btn btn-secondary btn-block" href="customer.php">Vai al tuo profilo</a>
                        </div>
                        <div class="col-md-4">
                            <a class="btn btn-secondary btn-block" href="suppliersList.php">Torna a fare spesa</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
