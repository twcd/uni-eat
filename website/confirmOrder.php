<?php
include "dbUtils.php";
include "secureSession.php";
include 'errorChecker.php';
sec_session_start();
login_check($mysqli);

if (!isCustomer()) {
    $errorMessage = "Devi essere autenticato come cliente per accedere a questa pagina.";
} else if (!getCart(getUserID(), $cart, $mysqli) or count($cart) == 0) {
    $errorMessage = "Il tuo carrello sembra essere vuoto";
}
?>

<!DOCTYPE html>
<html lang="it" dir="ltr">
    <head>
        <?php include "mainInclusions.php" ?>
        <script type="text/javascript" src="scripts/confirmOrder.js"></script>
        <title>Conferma ordine</title>
    </head>
    <body>
        <?php include "navbar.php" ?>
        <div class="main-container">
            <div class="content">
                <section>
                    <h1>Inserisci i dati per l'ordine</h1>
                    <div class="alert alert-info" role="alert">
                        Ricorda che puoi <strong>NON</strong> puoi effettuare ordini se l'orario di consegna ricade nei prossimi <strong>30 minuti</strong>.
                    </div>
                    <?php if (isset($errorMessage)) { ?>
                        <div class="alert alert-danger" role="alert">
                            <?php echo $errorMessage ?>
                        </div>
                    <?php } else { ?>
                        <?php if ($errNo == 1) { ?>
                            <div class="alert alert-danger" role="alert">
                                Mancano alcuni parametri.
                            </div>
                        <?php } else if ($errNo == 2) { ?>
                            <div class="alert alert-danger" role="alert">
                                Impossibile eseguire l'ordine.
                            </div>
                        <?php } ?>
                        <form id="orderform" action="processOrder.php" method="post">
                            <div class="form-group">
                                <label for="locationInput">Luogo consegna:</label>
                                <select class="form-control" name="location" id="locationInput" required>
                                    <option>Ingresso piano terra</option>
                                    <option>Ingresso Via Università</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="dateInput">Data di consegna</label>
                                <input id="dateInput" class="form-control" type="date" name="date" value="<?php echo date("Y-m-d") ?>" min="<?php echo date("Y-m-d") ?>" required/>
                            </div>
                            <div class="form-group">
                                <label for="timeInput">Orario di consegna</label>
                                <input id="timeInput" class="form-control" type="time" name="time" value="12:00" min="10:00" max="18:00" required/>
                            </div>
                            <input class="btn btn-primary" type="submit" value="Procedi con l'ordine">
                        </form>
                    <?php } ?>
                </section>
            </div>
        </div>
        <?php include "footer.php" ?>
    </body>
</html>
