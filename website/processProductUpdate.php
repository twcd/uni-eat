<?php
include "dbUtils.php";
include "secureSession.php";
include "fileSystemUtils.php";

sec_session_start();
login_check($mysqli);

if (!userLoggedIn()) {
    header('Location: login.php');
    exit;
}

$name = $_POST['name'];
$description = $_POST['description'];
$price = $_POST['price'];
$categoryID = $_POST['category'];
$productID = $_POST['productID'];

if (!isSupplier()) {
    header("Location: supplierProductModifier.php?error=3&productID=$productID");
    exit;
}

if (!isset($name, $description, $price, $categoryID, $productID, $categoryID)) {
    header("Location: supplierProductModifier.php?error=1&productID=$productID");
    exit;
}

if (!updateProduct($name, $description, $price, $categoryID, $productID, $mysqli)) {
    header("Location: supplierProductModifier.php?error=2&productID=$productID");
    exit;

}
if (!saveProductImage($productID, $_FILES['imageSrc'], '')) {
    header("Location: supplierProductModifier.php?error=4&productID=$productID");
    exit;
}

header('Location: supplier.php');
?>
